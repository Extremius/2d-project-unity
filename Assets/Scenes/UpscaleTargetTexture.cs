﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpscaleTargetTexture : MonoBehaviour
{
    [SerializeField] Camera textureDependetCam;
    [SerializeField] RawImage textureDependetRender;
    [SerializeField] RenderTexture getDescFrom;

    RenderTextureDescriptor desc;

    RenderTexture curText;
    Vector2Int screenSize;
    // Update is called once per frame
    void Update()
    {
        Vector2Int currentSize = new Vector2Int();
        currentSize.y = Screen.height;
        currentSize.x = Screen.width;

        if(screenSize != currentSize)
        {

            if(curText != null)
            {
                curText.Release();
            }

            desc = getDescFrom.descriptor;

            desc.height = currentSize.y;
            desc.width = currentSize.x;

            curText = new RenderTexture(desc);
            textureDependetCam.targetTexture = curText;
            textureDependetRender.texture = curText;

            screenSize = currentSize;
        }
    }

    void ApplyTextureToAll()
    {

    }
}

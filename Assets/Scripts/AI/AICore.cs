﻿#define USE_NATIVE_THREAD

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Stopwatch = System.Diagnostics.Stopwatch;


using Native;


namespace Actors.AI
{
    public enum AIState
    {
        Idle,
        MoveTo,
        AttackTo,
        Thinking,
    }
    public sealed class AICore : MonoBehaviour, INativeAILoop
    {

#region consts
        private const double secondDelayForLoop = 0.05;
#endregion

#region fields
        [SerializeField] private GameObject root;
        [SerializeField] private AIState aiState;


        private List<IAIModule> modules;
        private Stopwatch loopWatch = Stopwatch.StartNew();
#endregion

#region props
        public AIState State => aiState;
        public GameObject Root => root;
#endregion

#region methods
        private void InitModules()
        {
            List<IAIModule[]> allModules = new List<IAIModule[]>();
            int totalcount =0;

            var childCount = base.transform.childCount;
            for(int i =0;i<childCount;i++)
            {
                var arr = base.transform.GetChild(i).GetComponents<IAIModule>();
                totalcount += arr.Length;
                allModules.Add(arr);
            }

            modules = new List<IAIModule>(totalcount);
            foreach(var mdlarr in allModules)
            {
                foreach(var mdl in mdlarr)
                {
                    Attach(mdl);
                }
            }

            InitInternalBuffer();
            canAccessToModules = true;
            FirstCall();
            canAccessToModules = false;
        }

        public void Attach(IAIModule mdl)
        {
            modules.Add(mdl);
            mdl.AttachTo(this);
        }
        public void Deattach(IAIModule mdl)
        {
            if (modules.Remove(mdl))
            {
                mdl.AttachTo(null);
            }
        }
#endregion

#region internal methods
        private bool canAccessToModules = false;
        internal IAIModule GetModuleInterface(string name)
        {
            if (!canAccessToModules)
                throw new AccessViolationException("Нарушение доступа");

            foreach(var mdl in internalBuffer)
            {
                if (mdl == null) continue;
                if (mdl.moduleName.Equals(name))
                    return mdl;
            }
            return null;
        }
        internal T GetModule<T>(string name) where T : class, IAIModule
        {
            if (!canAccessToModules)
                throw new AccessViolationException("Нарушение доступа");

            foreach (var mdl in internalBuffer)
            {
                if(mdl is T tmdl)
                {
                    if (name is null)
                        return tmdl;
                    else if (name.Equals(mdl.moduleName))
                        return tmdl;
                }
            }

            return null;
        }
        internal T GetModule<T>() where T : class, IAIModule => GetModule<T>(null);
#endregion

#region behaviour
        private Comparer _comparer = new Comparer();
        private IAIModule[] internalBuffer = new IAIModule[0];
        private void InitInternalBuffer()
        {
            if (internalBuffer.Length != modules.Count)
                internalBuffer = new IAIModule[modules.Count];

            modules.Sort(_comparer);
            int iter = 0;
            for(int i =0;i<modules.Count;i++)
            {
                if(modules[i]!=null)
                {
                    internalBuffer[iter++] = modules[i];
                }
            }

            for(int i = iter;i<internalBuffer.Length;i++)
            {
                internalBuffer[i] = null;
            }
        }

        private void FirstCall()
        {
            foreach (var mdl in internalBuffer)
            {
                if (mdl == null)
                    continue;
                else if (mdl.Core == this)
                    mdl.AIStart();
            }
        }
        private void NativeAILoop()
        {

            //work with internalBuffer
            InitInternalBuffer();
            canAccessToModules = true;

            //FirstCall(); removed in Start() call 
            try
            {
                foreach (var mdl in internalBuffer)
                {
                    if (mdl == null)
                        continue;
                    else if (mdl.Core == this)
                    {
                        CallAILoop(mdl);
                    }
                }
            }
            catch(Exception e)
            {
                Debug.LogException(e);
            }

            canAccessToModules = false;
        }

        //support for NotImplementedException throws
        private void CallAILoop(IAIModule mdl)
        {
            try
            {
                if(mdl.Enabled)
                mdl.AILoop();
            }
            catch(NotImplementedException)
            {
                return;
            }
        }
#endregion

#region unity callback
        private void Start()
        {
            InitModules();

#if USE_NATIVE_THREAD
            NativeHelper.AddNativeAI(this);
#endif
        }

#if !USE_NATIVE_THREAD
        private void Update()
        {
            if (loopWatch.Elapsed.TotalSeconds >= secondDelayForLoop)
            {
                NativeAILoop();

                loopWatch.Restart();
            }
        }
#endif
#if USE_NATIVE_THREAD
        void FixedUpdate()
        {
            _transform = new NativeTransform(Root.transform);
        }
#endif
#endregion
        #region Native support
        public double DeltaTime => _deltaTime;
        private double _deltaTime;

        NativeTransform _transform;
        public new NativeTransform transform => _transform;

        void INative.Start() { }
        void INative.End(NativeLoop loop) { }
        void INative.Update(NativeLoop loop)
        {
            _deltaTime = loop.deltaTime;
           
            NativeAILoop();
        }
        #endregion

        #region comparer
        private class Comparer : IComparer<IAIModule>
        {
            int IComparer<IAIModule>.Compare(IAIModule x, IAIModule y)
            {
                if (x == null && y == null)
                    return 0;
                else if (x == null && y != null)
                    return 1;
                else if (x != null && y == null)
                    return -1;

                if (x.Priority > y.Priority)
                    return -1;
                else if (x.Priority < y.Priority)
                    return 1;
                else
                    return 0;
            }
        }
#endregion
    }

    public interface IAIModule
    {
        /// <summary>
        /// Активен модуль или нет
        /// </summary>
        bool Enabled { get; set; }
        /// <summary>
        /// Имя модуля
        /// </summary>
        string moduleName { get; }
        /// <summary>
        /// Приоритет исполнения модуля
        /// </summary>
        int Priority { get; set; }
        /// <summary>
        /// Ядро модуля
        /// </summary>
        AICore Core { get; }
        /// <summary>
        /// Вызывается при присоединении модуля к ядру
        /// </summary>
        /// <param name="core"></param>
        void AttachTo(AICore core);

        /// <summary>
        /// Вызывается каждый раз из ядра подобно MonoBehaviour.Update()
        /// </summary>
        void AILoop();
        /// <summary>
        /// Вызывается один раз из ядра подобно MonoBehaviour.Start()
        /// </summary>
        void AIStart();
    }

    public readonly struct NativeTransform
    {
        public readonly Vector3 position;
        public readonly Vector3 localPosition;
        public readonly Quaternion rotation;
        public readonly Quaternion localRotation;
        public readonly Vector3 localScale;
        public readonly Matrix4x4 worldToLocalMatrix;
        public readonly Matrix4x4 localToWorldMatrix;

        private readonly int hash;

        public NativeTransform(Transform tr)
        {
            position = tr.position;
            localPosition = tr.localPosition;
            rotation = tr.rotation;
            localRotation = tr.localRotation;
            localScale = tr.localScale;
            worldToLocalMatrix = tr.worldToLocalMatrix;
            localToWorldMatrix = tr.localToWorldMatrix;
            hash = tr.GetHashCode();
        }

        public static bool operator != (NativeTransform left, NativeTransform right)
        {
            return left.hash != right.hash;
        }

        public static bool operator == (NativeTransform left, NativeTransform right)
        {
            return left.hash == right.hash;
        }

        public override int GetHashCode()
        {
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj is NativeTransform tr)
            {
                return hash == tr.hash;
            }
            else return false;
        }
    }
}

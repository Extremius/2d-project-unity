﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Control.Injectors;
using System;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace Actors.AI
{
    public class EnemySimpleAI : MonoBehaviour
    {
        #region fields
        [SerializeField] private VisualModule visualModule;
        private IInputControl[] inputs;
        private IternalInjector injector;
        Stopwatch aiLoopWatch = Stopwatch.StartNew();
        #endregion

        #region unityCallback
        private void Start()
        {
            inputs = GetComponents<IInputControl>();
            injector = new IternalInjector(this);

            //AI
            seeObjects = visualModule.GetAllSeeObjects;
        }

        private void FixedUpdate()
        {
            if (aiLoopWatch.ElapsedMilliseconds > 100)
            {
                AILoop();
                aiLoopWatch.Restart();
            }
            MoveLoop();
        }
        #endregion

        #region AILoop
        #region AI fields
        private Vector2 moveDirection;
        [SerializeField] private float jumpDistance = 5;
        Stopwatch jumpDelay = Stopwatch.StartNew();

        private const string moveAxis = MoveBehaviour.moveAxis;
        private const string jumpAxis = MoveBehaviour.jumpAxis;
        private IReadOnlyList<VisualModule.CollisionData> seeObjects;
        #endregion
        private void AILoop()
        {
            if (seeObjects.Count < 1)
            {
                moveDirection = Vector2.zero;
                return;
            }

            float distance = float.MaxValue;
            GameObject selected = null;
            foreach(var obj in seeObjects)
            {
                if (obj.self == null)
                    continue;

                var curDistance = Vector2.Distance(transform.position, obj.self.transform.position);
                if (curDistance < distance)
                {
                    distance = curDistance;
                    selected = obj.self;
                }
            }
            if(selected == null)
            {
                moveDirection = Vector2.zero;
                return;
            }


            var distanceVector2 = (Vector2)selected.transform.position - (Vector2)transform.position;
            if (distanceVector2.x > 0)
                moveDirection.x = 1;
            else if (distanceVector2.x < 0)
                moveDirection.x = -1;
            else
                moveDirection.x = 0;

            if(distance <= jumpDistance)
            {
                moveDirection.y = 1;
            }
            else
                moveDirection.y = 0;
            //Write inputs
            
        }

        private void MoveLoop()
        {
            var moveInject = new Inject(moveAxis, moveDirection.x);
            var jumpInject = new Inject(jumpAxis, moveDirection.y);

            foreach (var inj in inputs)
            {
                inj.OnInject(moveInject);
                inj.OnInject(jumpInject);
            }
        }

        #endregion
        private sealed class IternalInjector : InjectorManager
        {
            public readonly EnemySimpleAI root;

            public IternalInjector(EnemySimpleAI root)
            {
                this.root = root;
                buffer = null;
            }

            #region overrides
            public override void Inject(Inject newInject)
            {
                foreach (var input in root.inputs)
                    input.OnInject(newInject);
            }

            public override bool GetInject(out Inject inject)
            {
                throw new NotSupportedException("Метод на данном экземпляре не поддерживается");
            }
            #endregion
        }
    }
}

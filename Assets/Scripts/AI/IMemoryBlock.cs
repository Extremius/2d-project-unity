﻿using System;

namespace Actors.AI
{
    public interface IMemoryBlock
    {
        DateTime CreationTime { get; }
        int FieldsCount { get; }
        bool IsValid { get; }
        string Name { get; }

        bool Delete(string fieldName);
        bool Delete(string fieldName, out object mem);
        bool Exists(string fieldName);
        bool Read<T>(string fieldName, out T mem);
        bool Read_Raw(string fieldName, out object mem);
        void Write(string fieldName, object mem);
    }
}
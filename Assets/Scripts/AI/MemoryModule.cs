﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace Actors.AI
{
    public static class MemoryAddress
    {
        public const string global = "global";
    }

    public class MemoryModule : MonoBehaviour, IAIModule
    {

        #region gc
        private static Stopwatch internalGCTime = Stopwatch.StartNew();
        #endregion

        #region fields
        private Dictionary<string, MemoryBlock> memory = new Dictionary<string, MemoryBlock>();
        #endregion

        #region public access
        public int MemoryBlockCount => memory.Count;
        public int TotalFieldsCount
        {
            get
            {
                int totalCount = 0;
                foreach(var pair in memory)
                {
                    totalCount += pair.Value.FieldsCount;
                }
                return totalCount;
            }
        }
        public bool ReadBlock(string address, out IMemoryBlock block)
        {
            if(memory.TryGetValue(address, out var blk))
            {
                try
                {
                    block = blk.GetAccess();
                    return true;
                }
                catch(TimeoutException)
                {
                    memory.Remove(address);
                    block = default;
                    return false;
                }
            }
            block = default;
            return false;
        }
        /// <summary>
        /// Создает новый блок памяти по указаному абстрактному адресу
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public IMemoryBlock CreateBlock(string address)
        {
            if (memory.ContainsKey(address))
            {
                if (memory[address].IsValid)
                    throw new ArgumentException($"Блок памяти по адресу {address} уже существует");
                else
                    memory.Remove(address);
            }

            return internal_CreateBlock(address, 60d, null);
        }
        /// <exception cref="ArgumentException"></exception>
        public IMemoryBlock CreateBlock(string address, double lifetime)
        {
            if (memory.ContainsKey(address))
            {
                if (memory[address].IsValid)
                    throw new ArgumentException($"Блок памяти по адресу {address} уже существует");
                else
                    memory.Remove(address);
            }

            return internal_CreateBlock(address, lifetime, null);
        }
        /// <exception cref="ArgumentException"></exception>
        public IMemoryBlock CreateBlock(string address, double lifetime, IDictionary<string, object> fields)
        {
            if (memory.ContainsKey(address))
            {
                if (memory[address].IsValid)
                    throw new ArgumentException($"Блок памяти по адресу {address} уже существует");
                else
                    memory.Remove(address);
            }

            return internal_CreateBlock(address, lifetime, fields);
        }
        #endregion

        #region internal methods
        private IMemoryBlock internal_CreateBlock(string address, double lifeTime, IDictionary<string, object> internal_memory)
        {
            var newBlock = new MemoryBlock(address, lifeTime, internal_memory);
            memory.Add(address, newBlock);
            return newBlock as IMemoryBlock;
        }

        private void gc_clear()
        {
            if (MemoryBlockCount < 1)
                return;

            List<string> unvalid = new List<string>(MemoryBlockCount);

            foreach(var pair in memory)
            {
                if (!pair.Value.IsValid)
                    unvalid.Add(pair.Key);
            }

            foreach(var key in unvalid)
            {
                memory.Remove(key);
            }

            gcTime.Restart();
            if (internalGCTime.ElapsedMilliseconds > 60_000)
            {
                GC.Collect();
                internalGCTime.Restart();
            }
        }

        Stopwatch gcTime = Stopwatch.StartNew();
        void Update()
        {
            if (gcTime.ElapsedMilliseconds > 60_000)
                gc_clear();
        }
        #endregion

        #region IAI
        new bool enabled = true;
        bool IAIModule.Enabled { get => enabled; set => enabled = value; }
        string IAIModule.moduleName => name;

        int IAIModule.Priority { get; set; } = 0;

        AICore IAIModule.Core => core;
        AICore core;

        void IAIModule.AILoop()
        {
            
        }

        void IAIModule.AIStart()
        {

        }

        void IAIModule.AttachTo(AICore core)
        {
            this.core = core;
        }
        #endregion

        private struct MemoryBlock : IMemoryBlock
        {
            //block name
            public string Name { get; }

            //live time
            private readonly DateTime timeCreated;
            private Stopwatch accessWatch;
            private readonly TimeSpan lifeTime;

            private IDictionary<string, object> fields;

            #region time access
            public DateTime CreationTime => timeCreated;
            public bool IsValid => accessWatch.Elapsed <= lifeTime;

            internal IMemoryBlock GetAccess()
            {
                if (IsValid)
                {
                    accessWatch.Restart();
                    return this as IMemoryBlock;
                }
                else throw new TimeoutException();
            }
            #endregion

            #region memory access
            public bool Read_Raw(string fieldName, out object mem)
            {
                if (fields.TryGetValue(fieldName, out var val))
                {
                    mem = val;
                    return true;
                }
                mem = null;
                return false;
            }
            public bool Read<T>(string fieldName, out T mem)
            {
                if (fields.TryGetValue(fieldName, out var val))
                {
                    if (val is T tval)
                    {
                        mem = tval;
                        return true;
                    }
                }

                mem = default;
                return false;
            }
            public bool Exists(string fieldName) => fields.ContainsKey(fieldName);
            public int FieldsCount => fields.Count;
            public void Write(string fieldName, object mem)
            {
                if (fields.ContainsKey(fieldName))
                {
                    fields[fieldName] = mem;
                }
                else fields.Add(fieldName, mem);
            }
            public bool Delete(string fieldName, out object mem)
            {
                if (Read_Raw(fieldName, out mem))
                {
                    return fields.Remove(fieldName);
                }
                else
                {
                    mem = null;
                    return false;
                }
            }
            public bool Delete(string fieldName)
            {
                return fields.Remove(fieldName);
            }
            #endregion

            #region factory
            public MemoryBlock(string name, double lifeTimeSec, IDictionary<string, object> fields = null)
            {
                Name = name;
                timeCreated = DateTime.UtcNow;
                accessWatch = Stopwatch.StartNew();
                lifeTime = new TimeSpan((long)(lifeTimeSec * 10_000_000));

                if(fields != null)
                {
                    this.fields = fields;
                }
                else
                {
                    this.fields = new Dictionary<string, object>();
                }
            }
            #endregion
        }

        
    }


}

﻿

using Control.Injectors;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace Actors.AI
{
    public class AI_GoToTarget : MonoBehaviour, IAIModule
    {
        #region debug
#if DEBUG_AI
        [SerializeField] GameObject debugTargetLastPositionPref;
        private void SetObject(Vector2 pos)
        {
            debugTargetLastPositionPref.transform.position = pos;
            debugTargetLastPositionPref.SetActive(true);
        }
        private void DisableObject() => debugTargetLastPositionPref.SetActive(false);
#endif
#endregion

        #region fields
        private IMemoryBlock memBlock;
        private Vector2 moveDirection;
        [SerializeField] private float jumpDistance = 5;
        Stopwatch jumpDelay = Stopwatch.StartNew();

        private const string moveAxis = MoveBehaviour.moveAxis;
        private const string jumpAxis = MoveBehaviour.jumpAxis;

        private MoveBehaviour moveComponent;
        private IternalInjector injector;
        #endregion

        #region IAI
        new bool enabled = true;
        bool IAIModule.Enabled { get => enabled; set => enabled = value; }
        string IAIModule.moduleName => name;

        int IAIModule.Priority { get => priority; set => priority = value; }
        [SerializeField] private int priority;

        AICore IAIModule.Core => core;
        AICore core;

        void IAIModule.AILoop()
        {
            if(core.GetModule<MemoryModule>().ReadBlock(AI_TargetSeek.selectedTarget, out memBlock))
            {
                if(memBlock.Read<Vector3>("lastPosition", out var lastPosition))
                {
                    if(memBlock.Read<bool>("isSee", out var isSee))
                    {
                        var selfTransform = core.transform;
#if DEBUG_AI
                        SetObject(lastPosition); 
#endif
                        if (!isSee) //Не видит но помнит
                        {
                            
                            var delta = lastPosition.x - selfTransform.position.x;//pivot core.root
                            if (delta > 0)
                                moveDirection.x = 1;
                            else if (delta < 0)
                                moveDirection.x = -1;

                            var distance = Vector2.Distance(lastPosition, selfTransform.position);
                            IsJump(jumpDistance * 1.5f);

                            if (distance <= 1f)
                            {
                                moveDirection = Vector2.zero;
                                memBlock.Delete(nameof(lastPosition));
#if DEBUG_AI
                                DisableObject();
#endif
                            }

                            //Выход
                            return;
                        }
                        else //Видит
                        {
                            var distanceVector2 = (Vector2)lastPosition - (Vector2)selfTransform.position;
                            if (distanceVector2.x > 0)
                                moveDirection.x = 1;
                            else if (distanceVector2.x < 0)
                                moveDirection.x = -1;
                            else
                                moveDirection.x = 0;

                            IsJump(distanceVector2.magnitude);

                            //Выход
                            return;
                        }
                    }
                }
            }

            //Выполнится в случае неудачного чтения данных
#if DEBUG_AI
            DisableObject();
#endif
            moveDirection = Vector2.zero;

            void IsJump(float jmpDis)
            {
                if (jmpDis <= jumpDistance)
                {
                    moveDirection.y = 1;
                }
                else
                    moveDirection.y = 0;

                //wall jump
                if (moveDirection.x != 0)
                {
                    var vel = moveComponent.Velocity;
                    if (Mathf.Round(vel.x) == 0)
                    {
                        moveDirection.y = 1;
                    }
                }
            }
        }

        void IAIModule.AIStart()
        {
            moveComponent = core.Root.GetComponent<MoveBehaviour>();
        }

        void IAIModule.AttachTo(AICore core)
        {
            this.core = core;
            if(core == null)
            {
                enabled = false;
                return;
            }
            injector = new IternalInjector(core.Root.GetComponents<IInputControl>());
        }
#endregion

#region methods
        private void MoveLoop()
        {
            var moveInject = new Inject(moveAxis, moveDirection.x);
            var jumpInject = new Inject(jumpAxis, moveDirection.y);

            injector.Inject(moveInject);
            injector.Inject(jumpInject);
        }

#region unityCallback
        private void Start()
        {
#if DEBUG_AI
            debugTargetLastPositionPref = Instantiate(debugTargetLastPositionPref);
            debugTargetLastPositionPref.name = $"point {name}";
            debugTargetLastPositionPref.SetActive(false);
#endif
            //AI
            //seeObjects = visualModule.GetAllSeeObjects;

        }

        private void FixedUpdate()
        {
            MoveLoop();
        }
#endregion

#endregion
        private sealed class IternalInjector : InjectorManager 
        {

            private IInputControl[] inputs;
            public IternalInjector(IInputControl[] inputs)
            {
                buffer = null;
                this.inputs = inputs;
            }

#region overrides
            public override void Inject(Inject newInject)
            {
                foreach (var input in inputs)
                    input.OnInject(newInject);
            }

            public override bool GetInject(out Inject inject)
            {
                throw new NotSupportedException("Метод на данном экземпляре не поддерживается");
            }
#endregion
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors.Weapons;
using Control;
using Control.Injectors;
using Native;

namespace Actors.AI
{
    public class AI_Shoot : MonoBehaviour, IAIModule
    {
        [SerializeField] WeaponHolder holder;
        //[SerializeField] RawCursorPointer pointer;
        [SerializeField] int priority;
        [SerializeField] float distance = 15;

        private new bool enabled = true;
        AICore core;

        bool IAIModule.Enabled { get => enabled; set => enabled = value; }

        string IAIModule.moduleName => gameObject.name;

        int IAIModule.Priority { get => priority; set => priority =value; }

        AICore IAIModule.Core => core;

        void IAIModule.AILoop()
        {
            var mem = core.GetModule<MemoryModule>();
            if (mem.ReadBlock(AI_TargetSeek.selectedTarget, out var targetInfo))
            {
                if(mem.ReadBlock(RawCursorPointer.pointer_namespace, out var pointer))
                {
                    targetInfo.Read<bool>(AI_TargetSeek.target_isSee, out var isSee);
                    targetInfo.Read<Vector3>(AI.AI_TargetSeek.target_lastPosition, out var lastPosition);

                    if(isSee)
                    {
                        pointer.Write(RawCursorPointer.pointer_position, lastPosition);

                        if (Vector2.Distance(core.transform.position, lastPosition) <= distance)
                            press = 1;
                        else
                            press = 0;
                    }
                    else
                    {
                        var direction = lastPosition - core.transform.position;
                        direction.y = 0;
                        direction.Normalize();
                        pointer.Write(RawCursorPointer.pointer_position, direction);
                        press = 0;
                    }
                }
                else core.GetModule<MemoryModule>().CreateBlock(RawCursorPointer.pointer_namespace);
            }
        }

        void IAIModule.AIStart()
        {
            core.GetModule<MemoryModule>().CreateBlock(RawCursorPointer.pointer_namespace);
            input = holder;
        }

        void IAIModule.AttachTo(AICore core)
        {
            this.core = core;
        }

        #region behaviour
        
        IInputControl input;
        float press = 0;
        void FixedUpdate()
        {
            input?.OnInject(new Inject(WeaponHolder.fire1, press));
        }
        #endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Control.Injectors;
using System;
using Stopwatch = System.Diagnostics.Stopwatch;
namespace Actors.AI
{
    public class AI_SimpleBehaviour : MonoBehaviour, IAIModule
    {
        #region consts
        private const string mem_target = nameof(AI_SimpleBehaviour) + ":target";
        #endregion

        #region debug
        [SerializeField] GameObject debugTargetLastPositionPref;
        private void SetObject(Vector2 pos)
        {
            debugTargetLastPositionPref.transform.position = pos;
            debugTargetLastPositionPref.SetActive(true);
        }
        private void DisableObject() => debugTargetLastPositionPref.SetActive(false);
        #endregion

        #region fields
        //[SerializeField] private VisualModule visualModule;
        private MoveBehaviour moveComponent;
        private IInputControl[] inputs;
        private IternalInjector injector;
        private MemoryModule mem;
        #endregion

        #region AI fields
        private Vector2 moveDirection;
        [SerializeField] private float jumpDistance = 5;
        Stopwatch jumpDelay = Stopwatch.StartNew();

        private const string moveAxis = MoveBehaviour.moveAxis;
        private const string jumpAxis = MoveBehaviour.jumpAxis;
        private IReadOnlyList<VisualModule.CollisionData> seeObjects;
        #endregion

        #region unityCallback
        private void Start()
        {
            injector = new IternalInjector(this);
            debugTargetLastPositionPref = Instantiate(debugTargetLastPositionPref);
            debugTargetLastPositionPref.name = $"point {name}";
            debugTargetLastPositionPref.SetActive(false);
            //AI
            //seeObjects = visualModule.GetAllSeeObjects;
        }

        private void FixedUpdate()
        {
            MoveLoop();
        }
        #endregion

        #region IAI
        new bool enabled = true;
        bool IAIModule.Enabled { get => enabled; set => enabled = value; }
        string IAIModule.moduleName => name;

        int IAIModule.Priority { get; set; } = 10;

        AICore IAIModule.Core => _core;
        private AICore _core;

        //Update
        void IAIModule.AILoop()
        {
            //prepare
            IMemoryBlock memBlock;
            if (!mem.ReadBlock(mem_target, out memBlock))
            {
                Debug.Log($"memBlock was recreating on {_core.Root.name}");
                memBlock = mem.CreateBlock(mem_target);
                DisableObject();
            }
            Transform selfTransform = _core.Root.transform;
            float distance = float.MaxValue;
            Vector2 lastPosition = default;

            if (seeObjects.Count < 1)
            {
                //Если не видно цели
                if(memBlock.Read(nameof(lastPosition), out lastPosition))
                {
                    /*DEBUG*/
                    SetObject(lastPosition);

                    var delta = lastPosition.x - selfTransform.position.x;//pivot core.root
                    if (delta > 0)
                        moveDirection.x = 1;
                    else if (delta < 0)
                        moveDirection.x = -1;

                    distance = Vector2.Distance(lastPosition, selfTransform.position);
                    IsJump(jumpDistance*1.5f);

                    if (distance <= 1f)
                    {
                        moveDirection = Vector2.zero;
                        memBlock.Delete(nameof(lastPosition));
                        DisableObject();
                    }
                }
                else
                {
                    moveDirection = Vector2.zero;
                }
                return;
            }

           
            //Если видно цель
            GameObject selected = null;
            foreach (var obj in seeObjects)
            {
                if (obj.self == null)
                    continue;

                var curDistance = Vector2.Distance(selfTransform.position, obj.self.transform.position);
                if (curDistance < distance)
                {
                    distance = curDistance;
                    selected = obj.self;
                }
            }
            if (selected == null)
            {
                moveDirection = Vector2.zero;
                return;
            }

            //Запись позиции цели
            lastPosition = selected.transform.position;
            memBlock.Write(nameof(lastPosition), lastPosition);
            /*DEBUG*/
            SetObject(lastPosition);

            var distanceVector2 = (Vector2)selected.transform.position - (Vector2)selfTransform.position;
            if (distanceVector2.x > 0)
                moveDirection.x = 1;
            else if (distanceVector2.x < 0)
                moveDirection.x = -1;
            else
                moveDirection.x = 0;

            IsJump(distance);

            void IsJump(float jmpDis)
            {
                if (jmpDis <= jumpDistance)
                {
                    moveDirection.y = 1;
                }
                else
                    moveDirection.y = 0;

                //wall jump
                if(moveDirection.x!=0)
                {
                    var vel = moveComponent.Velocity;
                    if(Mathf.Round(vel.x) == 0)
                    {
                        moveDirection.y = 1;
                    }
                }
            }
        }

        //Start
        void IAIModule.AIStart()
        {
            seeObjects = _core.GetModule<VisualModule>("eyes").GetAllSeeObjects;
            mem = _core.GetModule<MemoryModule>();
            mem.CreateBlock(mem_target);
            moveComponent = _core.Root.GetComponent<MoveBehaviour>();
        }

        void IAIModule.AttachTo(AICore core)
        {
            _core = core;
            if(core == null)
            {
                gameObject.SetActive(false);
                return;
            }

            inputs = core.Root.GetComponents<IInputControl>();
            
        }
        #endregion

        #region methods
        private void MoveLoop()
        {
            var moveInject = new Inject(moveAxis, moveDirection.x);
            var jumpInject = new Inject(jumpAxis, moveDirection.y);

            foreach (var inj in inputs)
            {
                inj.OnInject(moveInject);
                inj.OnInject(jumpInject);
            }
        }

        #endregion
        private sealed class IternalInjector : InjectorManager
        {
            public readonly AI_SimpleBehaviour root;

            public IternalInjector(AI_SimpleBehaviour root)
            {
                this.root = root;
                buffer = null;
            }

            #region overrides
            public override void Inject(Inject newInject)
            {
                foreach (var input in root.inputs)
                    input.OnInject(newInject);
            }

            public override bool GetInject(out Inject inject)
            {
                throw new NotSupportedException("Метод на данном экземпляре не поддерживается");
            }
            #endregion
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Actors.AI
{
    public class AI_TargetSeek : MonoBehaviour, IAIModule
    {
        public const string selectedTarget = "global:selectedTarget";
        public const string target_transform = "transform";
        public const string target_isSee = "isSee";
        public const string target_lastPosition = "lastPosition";
        [System.Flags]
        public enum SelectOption
        {
            None =0x0,
            Health =0x1,
            Distance =0x2,
        }

        #region fields
        private IMemoryBlock memBlock;
        private IReadOnlyList<VisualModule.CollisionData> seeObjects;
        [SerializeField] SelectOption selectedOption;
        #endregion

        #region IAI
        new bool enabled = true;
        bool IAIModule.Enabled { get => enabled; set => enabled = value; }
        string IAIModule.moduleName => name;

        int IAIModule.Priority { get => priority; set => priority = value; }
        [SerializeField] private int priority;

        AICore IAIModule.Core => core;
        AICore core;

        void IAIModule.AILoop()
        {
            
            if (seeObjects.Count>0)
            {
                
                if(core.GetModule<MemoryModule>().ReadBlock(selectedTarget, out memBlock))
                {
                    memBlock.Read<GameObject>(target_transform, out var tr);
                    loop(tr);
                }
                else
                {
                    memBlock = core.GetModule<MemoryModule>().CreateBlock(selectedTarget);
                    loop(null);
                }
            }

            void loop(GameObject memTranfrom)
            {
                bool isFollow = false;

                //Нам нужно точное значение. Если блок памяти был только что создан, то там нет этого поля, слд-но isFollow должно быть равно false
                if (memBlock.Read<bool>(target_isSee, out var _isFollow))
                    isFollow = _isFollow;

                BEGIN_LOOP:
                //if (memTranfrom == null && !isFollow)
                if (!isFollow)
                {
                    //Поиск новой цели
                    var selfTransform = core.transform;
                    var distance = float.MaxValue;
                    lock (seeObjects)
                    {
                        foreach (var slct in seeObjects)
                        {


                            //if (slct.self == null) //unity null
                            if (slct.IsNull)
                                continue;

                            var curDistance = Vector2.Distance(selfTransform.position, slct.transform.position);
                            if (curDistance < distance)
                            {
                                distance = curDistance;
                                memTranfrom = slct.self;
                            }
                        }
                    }
                }
                else
                {
                    bool isSee = false;
                    lock (seeObjects)
                    {
                        foreach (var slct in seeObjects)
                        {
                            //if (slct.self == null) //unity null
                            if (slct.IsNull)
                                continue;

                            if ((object)slct.self == memTranfrom)
                            {
                                isSee = true;
                                break;
                            }
                        }
                    }

                    if (!isSee)
                    {
                        memTranfrom = null;
                        isFollow = false;
                        goto BEGIN_LOOP;
                    }
                }

                //Делегирование записи основному потоку
                Native.NativeHelper.MainThread.Delegate(delegate
                {
                    //запись самой цели, записывается так же значение null
                    memBlock.Write(target_transform, memTranfrom);
                    if (memTranfrom != null)
                    {
                        //если цель видно, обновляется последняя позиция
                        memBlock.Write(target_lastPosition, memTranfrom.transform.position);
                        memBlock.Write(target_isSee, true);
                    }
                    else
                    {
                        memBlock.Write(target_isSee, false);
                    }
                }, true);
            }
        }

        void IAIModule.AIStart()
        {
            memBlock = core.GetModule<MemoryModule>().CreateBlock(selectedTarget);
            seeObjects = core.GetModule<VisualModule>().GetAllSeeObjects;
        }

        void IAIModule.AttachTo(AICore core)
        {
            this.core = core;
        }
        #endregion

    }
}

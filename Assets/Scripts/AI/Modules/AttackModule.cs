﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;
using Control.Injectors;
using System;


namespace Actors.AI 

{

    public class AttackModule : MonoBehaviour, IAIModule
    {


        //private IternalInjector injector;

        public float trigerDistance;
        new bool enabled = true;
        bool IAIModule.Enabled { get => enabled; set => enabled = value; }
        string IAIModule.moduleName => name;

        int IAIModule.Priority { get => priority; set => priority = value; }
        [SerializeField] private int priority;

        AICore IAIModule.Core => core;

        AICore core;

        private IternalInjector injector;

        private IMemoryBlock memBlock;

         private SimpleAttack attacker;

        //////// injector.Inject(attackAxes);


        private const string attackAxes = SimpleAttack.fire1;





        void IAIModule.AILoop()
        {
            if (core.GetModule<MemoryModule>().ReadBlock(AI_TargetSeek.selectedTarget, out memBlock))
            {
                if (memBlock.Read<Vector3>("lastPosition", out var lastPosition))
                {
                    if (memBlock.Read<bool>("isSee", out var isSee))
                    {
                        //var selfTransform = core.Root.transform;
                        // SetObject(lastPosition);

                        // injector.Inject(new Inject(attackAxes,1));

                        //if(attacker.Watcher.Elapsed.TotalSeconds >= attacker.Reload )
                        // injector.Inject(new Inject(attackAxes, 1));

                        //AI_TargetSeek.selectedTarget.

                        var selfTransform = core.transform;

                        //var distanceVector2 = (Vector2)lastPosition - (Vector2)selfTransform.position;

                        var distance = Vector2.Distance(lastPosition, selfTransform.position);

                        if (isSee)
                        {
                            if (distance < trigerDistance && attacker.Watcher.Elapsed.TotalSeconds >= attacker.Reload)
                            {
                                delegateInject();
                            }
                        }
                        else
                        {
                            if (attacker.SpikesActive)
                            {
                                delegateInject();
                                Debug.Log("Spikes disabled");
                            }
                        }

                    }

                }

                void delegateInject()
                {
                    Native.NativeHelper.MainThread.Delegate(delegate { injector.Inject(new Inject(attackAxes, 1)); }, true);
                }

            }

        }


        void IAIModule.AIStart()
        {
            attacker = core.Root.GetComponent<SimpleAttack>();

            //Attacker = gameObject.GetComponentInParent<SimpleAttack>();
        }

        void IAIModule.AttachTo(AICore core)
        {
            this.core = core;
            if (core == null)
            {
                enabled = false;
                return;
            }
            injector = new IternalInjector(core.Root.GetComponents<IInputControl>());
        }


        private sealed class IternalInjector : InjectorManager
        {

            private IInputControl[] inputs;
            public IternalInjector(IInputControl[] inputs)
            {
                buffer = null;
                this.inputs = inputs;
            }

           
            public override void Inject(Inject newInject)
            {
                foreach (var input in inputs)
                {
                    if((UnityEngine.Object)input != null)
                    input.OnInject(newInject);
                }
            }

            public override bool GetInject(out Inject inject)
            {
                throw new NotSupportedException("Метод на данном экземпляре не поддерживается");
            }
            
        }

    }



}

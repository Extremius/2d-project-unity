﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enviroment;
using Stopwatch = System.Diagnostics.Stopwatch;
using System;

namespace Actors.AI
{
    public class VisualModule : MonoBehaviour, IAIModule
    {
        #region constants
        private const double raycastDelayUpdate_ms = 500;
        #endregion

        #region fields

        //[SerializeField] private GameObject RootObject;
        private GameObject RootObject => _core.Root;
        [SerializeField] private float radius;
        private CircleCollider2D colRange;
        [SerializeField, Tooltip("Ищет все объекты с одним или более указанными тегами")] private bool seekOnlyOneTag = false;
        [SerializeField] private Attributes seekObjectsWithAttributes;
        [SerializeField, Tooltip("Ищет объекты только в прямой видимости")] private bool lineVisual = false;
        [SerializeField, Tooltip("Позиция глаз объекта. Работает только при lineVisual = true")] private Transform eyeTransform;
        [SerializeField] private Attributes ignoreLineTrackingWithAttributes;
        
        #endregion

        #region internal fields
        [SerializeField] private List<CollisionData> allObjectsInRange = new List<CollisionData>(16);
        [SerializeField] private List<CollisionData> forwardLineSeeObjects = new List<CollisionData>(16);
        Stopwatch loopWatch = Stopwatch.StartNew();
        ContactFilter2D filter2D;
        #endregion

        #region props
        public IReadOnlyList<CollisionData> GetAllSeeObjects
        {
            get
            {
                if (lineVisual)
                {
                    return forwardLineSeeObjects as IReadOnlyList<CollisionData>;
                }
                else
                    return allObjectsInRange as IReadOnlyList<CollisionData>;
            }
        }
        new bool enabled = true;
        bool IAIModule.Enabled { get => enabled; set => enabled = value; }
        AICore IAIModule.Core => _core;
        private AICore _core;
        int IAIModule.Priority { get; set; } = int.MaxValue;
        string IAIModule.moduleName => name;
        #endregion

        #region unity callback
        private void Start()
        {
            colRange = gameObject.AddComponent<CircleCollider2D>();
            colRange.isTrigger = true;
            colRange.radius = radius;
            filter2D = new ContactFilter2D()
            {
                layerMask = LayerMask.NameToLayer("Default"),
                useTriggers = false,
                useOutsideDepth = true,
            };
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (RootObject == collision.gameObject)
                return;

            if(HasAllAttributes(collision.gameObject.GetComponent<ObjectAttribute>()))
            {
                allObjectsInRange.Add(new CollisionData(collision.gameObject, collision));
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (RootObject == collision.gameObject)
                return;

            allObjectsInRange.Remove(new CollisionData(collision.gameObject, collision));
        }

        private void Update()
        {
            if(loopWatch.Elapsed.TotalMilliseconds >= raycastDelayUpdate_ms)
            {
                SeekForwardLineObjects();
                loopWatch.Restart();
            }
        }

        private void OnDisable()
        {
            Debug.Log($"{name} module is off");

            forwardLineSeeObjects.Clear();
        }
        #endregion

        #region methods
        private bool HasAllAttributes(ObjectAttribute @object)
        {
            if (@object == null)
                return false;

            if(seekOnlyOneTag)
                return (@object.Get & seekObjectsWithAttributes) != Attributes.None;
            else
                return (@object.Get & seekObjectsWithAttributes) == seekObjectsWithAttributes;
        }

        private RaycastHit2D[] raycastHit = new RaycastHit2D[32];
        private int hitCount = 0;
        private void SeekForwardLineObjects()
        {
            lock (forwardLineSeeObjects)
            {
                forwardLineSeeObjects.Clear();
                var selfCollider = RootObject.GetComponent<Collider2D>();
                Transform selfEye = eyeTransform == null ? selfCollider.transform : eyeTransform;
                foreach (var inRangeObject in allObjectsInRange)
                {
                    if (inRangeObject.catchCollider == null)
                        continue;
                    // center
                    var targetPoint = inRangeObject.catchCollider.ClosestPoint(selfEye.position);
                    var boundsSizeYHalf = inRangeObject.catchCollider.bounds.size.y * 0.3f;
                    //var deltaY = inRangeObject.catchCollider.transform.position.y - selfEye.position.y;
                    var deltaY = selfEye.position.y - inRangeObject.catchCollider.transform.position.y;
                    targetPoint.y += deltaY;
                    targetPoint = inRangeObject.catchCollider.ClosestPoint(targetPoint);
                    var invertedDelta = 1f / deltaY;
                    invertedDelta = invertedDelta > boundsSizeYHalf ? boundsSizeYHalf : invertedDelta;
                    targetPoint.y -= invertedDelta;
                    var direction = (targetPoint - (Vector2)selfEye.position).normalized;
                    //hitCount = selfCollider.Raycast(direction, filter2D, raycastHit, radius);
                    hitCount = Physics2D.RaycastNonAlloc(selfEye.position, direction, raycastHit, radius);


                    //DEBUG
                    Debug.DrawRay(selfEye.position, direction.normalized * Vector2.Distance(inRangeObject.self.transform.position, selfEye.position), Color.blue, 0.5f);

                    bool isSee = false;
                    for (int i = 0; i < hitCount; i++)
                    {
                        if (raycastHit[i].collider.gameObject == RootObject)
                            continue;
                        if (raycastHit[i].collider.isTrigger)
                            continue;

                        if (raycastHit[i].collider != inRangeObject.catchCollider)
                        {
                            if (RequestIgnore(raycastHit[i].collider.GetComponent<ObjectAttribute>()))
                            {
                                isSee = false;
                                break;
                            }
                        }
                        else
                        {
                            isSee = true;
                            break;
                        }
                    }

                    if (isSee)
                    {
                        forwardLineSeeObjects.Add(new CollisionData(inRangeObject));
                    }
                }
            }

            bool RequestIgnore(ObjectAttribute obj)
            {
                if (obj == null)
                    return true;

                return !((obj.Get & ignoreLineTrackingWithAttributes) == ignoreLineTrackingWithAttributes);
            }
        }


        #endregion

        #region iaimodule implements
        void IAIModule.AttachTo(AICore core)
        {
            _core = core;
        }

        void IAIModule.AILoop() { }

        void IAIModule.AIStart() { }
        #endregion

        [System.Serializable]
        public readonly struct CollisionData
        {
            public GameObject self { get; }
            public Collider2D catchCollider { get; }
            public NativeTransform transform { get; }

            public bool IsNull { get; }
            public CollisionData(GameObject self, Collider2D catchCollider)
            {
                this.self = self;
                this.catchCollider = catchCollider;
                transform = new NativeTransform(self.transform);
                IsNull = self == null;
            }
            public CollisionData(CollisionData toUpdate)
            {
                this.self = toUpdate.self;
                this.catchCollider = toUpdate.catchCollider;

                transform = new NativeTransform(self.transform);
                IsNull = self == null;
            }
        }
    }
}

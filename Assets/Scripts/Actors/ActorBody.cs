﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using UnityEngine.Experimental;
using UnityEngine.Experimental.Rendering.Universal;

namespace Actors
{
    public class ActorBody : HealthBody, IStatsDependet
    {
        [SerializeField] UI.ShowDamage shower;
        [SerializeField] ActorValue armor;
        public ReadOnlyActorValue ArmorAccess => armor;
        [SerializeField] private double minResistance = 0.3d, maxResistance = 0.9d;
        [SerializeField]StaminaBody stam;

        //Info
        private double lastResistance, lastDamage;
        public double LastResistance => lastResistance;
        public double LastDamage => lastDamage;
        protected override void OnChangeHealth(GameObject source, double delta, bool isForce = false)
        {
            if (delta < 0)
            {
                if (!armor.IsMin)
                {
                    var armorPercent = armor.Percent;
                    if (armorPercent > maxResistance)
                        armorPercent = maxResistance;
                    else if (armorPercent < minResistance)
                        armorPercent = minResistance;

                    var deltaArmor = Math.Abs(delta * armorPercent);
                    if (deltaArmor > armor.Value)
                        deltaArmor = armor.Value;
                    armor.Value -= deltaArmor;

                    lastResistance = armorPercent;

                    delta += deltaArmor;
                }
                delta = stam.ChangeDamage(delta);
                health.Value += delta;
                lastDamage = delta;
                if (shower != null)
                    shower.PrintDamage(delta);
            }
            else
                base.OnChangeHealth(source, delta, isForce);

            if (!IsAlive)
                OnDie();
            //else
                //Debug.Log($"{name} health is {HealthAccess.Value}");
        }
        public void ChangeArmor(double value)
        {
            armor.Value += value;
        }
        public void SetArmor(double value) => armor.Value = value;
        private void OnDie()
        {
            Debug.Log($"{name} is die");
            if (CompareTag("Player"))
            {
                Enviroment.PlayerAccess.Self.ResetPositionScript.ResetSelf();
            }
            else
            {
                transform.Find("UI").gameObject.SetActive(false);
                //GetComponent<Collider2D>().enabled = false;
                //transform.Find("FootStamp").gameObject.SetActive(false);
                transform.Find("Spikes").gameObject.SetActive(false);
                StartCoroutine(DefaultKill(2f));
            }
        }

        IEnumerator DefaultKill(float destroyTime)
        {
            var light2d = GetComponentInChildren<Light2D>();
            var startIntensity = light2d.pointLightOuterRadius;
            var deltaIntencity = startIntensity / destroyTime; 
            var aicore = transform.Find("AI").GetComponent<AI.AICore>();
            //aicore.enabled = false;
            aicore.gameObject.SetActive(false);
            var startTime = Time.time;
            while((Time.time-startTime)<destroyTime)
            {
                var scale = transform.localScale;
                light2d.pointLightOuterRadius -= deltaIntencity * Time.deltaTime;
                scale -= new Vector3(0, (1 * Time.deltaTime) / destroyTime, 0);
                transform.localScale = scale;

                yield return new WaitForEndOfFrame();
                
            }

            Destroy(gameObject);
            Debug.Log($"{name} is destroyed");
        }

        public override void ResetHealth()
        {
            armor.Value = armor.MaxValue * 0.3;
            base.ResetHealth();
        }

        #region stats support
        static string[] dependetStats = new string[] 
        {
            GenericValues.Stats.constitution,
            GenericValues.Stats.addHealth,
            GenericValues.Stats.baseHealth,
            GenericValues.Stats.healthPerCon,
        };
        IReadOnlyCollection<string> IStatsDependet.DependetStats => dependetStats;
        bool IStatsDependet.RequestSync => false;
        void IStatsDependet.OnChangeUpdate(StatsBlock.Stat[] stats)
        {
            var con = stats[0];
            var addHealth = stats[1];
            var baseHealth = stats[2];
            var healthPerCon = stats[3];

            var newMaxhealth = (baseHealth.Value * (1 + (con.Value - con.Basic) * healthPerCon.Value)) + addHealth.Value;
            if (_health.MaxValue != newMaxhealth)
            {
                _health.MaxValue = newMaxhealth;
                armor.MaxValue = newMaxhealth * 3;
            }

        }
        #endregion

        #region OBSOLETE statsUpdate
        //[SerializeField] private StatsBlock block;//ссылка на статы
        //private void UpdateHealthValuesFromStats()
        //{
        //    //var con = block[GenericValues.Stats.constitution];
        //    //var addHealth = block[GenericValues.Stats.addHealth].Value;
        //    //var baseHealth = block[GenericValues.Stats.baseHealth].Value;
        //    //var healthPerCon = block[GenericValues.Stats.healthPerCon].Value;

        //    var newMaxhealth = (baseHealth.Value * (1 + (con.Value - con.Basic) * healthPerCon.Value)) + addHealth.Value;
        //    if (_health.MaxValue != newMaxhealth)
        //        _health.MaxValue = newMaxhealth;
        //}
        //private void CallUpdate(StatsBlock.Stat stat)
        //{
        //    if (stat.Root == block)
        //        UpdateHealthValuesFromStats();
        //    else stat.OnChangeHandler -= CallUpdate;
        //}
        //StatsBlock.Stat con, addHealth, baseHealth, healthPerCon;
        //void ApplyEvents()
        //{
        //    con = block[GenericValues.Stats.constitution];
        //    addHealth = block[GenericValues.Stats.addHealth];
        //    baseHealth = block[GenericValues.Stats.baseHealth];
        //    healthPerCon = block[GenericValues.Stats.healthPerCon];

        //    con.OnChangeHandler += CallUpdate;
        //    addHealth.OnChangeHandler += CallUpdate;
        //    healthPerCon.OnChangeHandler += CallUpdate;
        //    baseHealth.OnChangeHandler += CallUpdate;
        //}
        //void ClearEvents()
        //{
        //    con.OnChangeHandler -= CallUpdate;
        //    addHealth.OnChangeHandler -= CallUpdate;
        //    healthPerCon.OnChangeHandler -= CallUpdate;
        //    baseHealth.OnChangeHandler -= CallUpdate;

        //}
        //#endregion

        //#region unity
        //private void Start()
        //{
        //    block = GetComponent<StatsBlock>();
        //    ApplyEvents();
        //    UpdateHealthValuesFromStats();
        //}
        #endregion
    }
}

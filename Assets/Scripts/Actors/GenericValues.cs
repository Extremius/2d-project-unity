﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Actors
{
    public static class GenericValues
    {
        public static class Values
        {

        }

        public static class Stats
        {
            public const string strength = "generic.stat.strength";
            public const string agility = "generic.stat.agility";
            public const string intelligence = "generic.stat.intelligence";
            public const string constitution = "generic.stat.constitution";

            public const string attack = "generic.attack";
            public const string armor = "generic.armor";
            public const string evasion = "generic.evasion";
            public const string moveSpeed = "generic.movespeed";
            public const string attackSpeed = "generic.attackspeed";

            public const string addHealth = "generic.additional.health";
            public const string baseHealth = "generic.base.health";
            public const string healthPerCon = "generic.mod.health_per_constitution";

            public const string addStamina = "generic.additional.stamina";
            public const string baseStamina = "generic.base.stamina";
            public const string staminaPerCon = "generic.mod.stamina_per_constitution";

            public const string lvl = "generic.progression.level";
            public const string exp = "generic.progression.expirience";
            public const string points = "generic.progression.points";

            public const string expOnKill = "generic.reward.expirience";
        }
    }
}

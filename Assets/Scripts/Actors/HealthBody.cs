﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace Actors
{
    public abstract class HealthBody : MonoBehaviour
    {
        public const double invincbleTime = 0.1;
        #region fields
        public event System.Action<HealthBody, GameObject> OnDieHandler;
        //HealthBody - цель, которую убили
        //GameObject - убийца
        private Stopwatch watcher, invincbleWatcher = Stopwatch.StartNew();
        [SerializeField] protected ActorValue _health;
        protected ref ActorValue health
        {
            get
            {
                PassiveUpdate();
                return ref _health;
            }
        }
        #endregion

        #region Public Access
      
        IEnumerator routine()
        {
            yield return new WaitForSecondsRealtime(0.1f);
            invincbleWatcher.Restart();
        }

        public virtual void ChangeHealth(GameObject source, double delta, bool isForce = false)
        {
            bool isoldAlive = IsAlive;

            if (IsAlive && !isForce)
            {
                if(delta<0)
                {
                    if (invincbleWatcher.Elapsed.TotalSeconds <= invincbleTime)
                        return;
                    else
                        StartCoroutine(routine());
                    
                }
                //health.Value += delta;
                OnChangeHealth(source, delta, isForce);
                if (!IsAlive)
                    OnDieHandler?.Invoke(this, source);
                
            }

            //timer control
            if(!IsAlive)
            {
                watcher.Reset();
            }
            else if(!isoldAlive)
            {
                watcher.Start();
            }
        }

        public virtual void ResetHealth()
        {
            _health.Value = _health.MaxValue;
            
            if(watcher != null)
            watcher.Restart();
        }

        public ReadOnlyActorValue HealthAccess => health;
        public bool IsAlive => !health.IsMin;
        #endregion

        #region protectedUpdate
        //Callback
        protected virtual void OnChangeHealth(GameObject source, double delta, bool isForce = false)
        {
            health.Value += delta;
        }
        #endregion

        #region PassiveUpdate
        private void PassiveUpdate()
        {
            if (watcher is null)
                watcher = Stopwatch.StartNew();
            if (!_health.IsMin)
            {
                var elapsedTime = watcher.Elapsed.TotalSeconds;
                _health.Update(elapsedTime);
            }

            watcher.Restart();
        }
        #endregion


        
    }

    /// <summary>
    /// Изменяема, ренджированая, обновляемая
    /// </summary>
    [System.Serializable] public struct ActorValue
    {
        [SerializeField]private double minValue, maxValue, value, derivate;

        #region props
        public double Derivate { get => derivate; set => derivate = value; }
        public double Value
        {
            get => value;
            set
            {
                //value is arg
                //this.value is field
                if (value > maxValue)
                    value = maxValue;
                else if (value < minValue)
                    value = minValue;
                this.value = value;
            }
        }
        public double MaxValue
        {
            get => maxValue;
            set
            {
                maxValue = value;
                if (value < minValue)
                    SwapMinMax();

                if (Value > maxValue)
                    Value = maxValue;
            }
        }

        public double MinValue
        {
            get => minValue;
            set
            {
                minValue = value;
                if (value > maxValue)
                    SwapMinMax();

                if (Value < minValue)
                    Value = minValue;
            }
        }

        public double Range => MaxValue - MinValue;
        public double Percent => (Value - MinValue) / Range;

        public bool IsMin => value == minValue;
        public bool IsMax => value == maxValue;
        #endregion

        #region private
        private void SwapMinMax()
        {
            var t = minValue;
            minValue = maxValue;
            maxValue = t;
        }
        private void CorrectValid()
        {
            if(minValue >= maxValue)
                SwapMinMax();

            if (value < minValue)//value is field
                Value = minValue;//Value is prop
            else if (value > maxValue)
                Value = maxValue;
        }
        #endregion

        #region methods
        /// <summary>
        /// Изменяет текущее значение на derivate с коэф k
        /// </summary>
        /// <param name="k"></param>
        public void Update(double k)
        {
            Value += derivate * k;
        }
        #endregion

        #region fabric
        public ActorValue(double minVal, double maxVal, double curVal)
        {
            this.minValue = minVal;
            this.maxValue = maxVal;
            this.value = curVal;
            this.derivate = 0;

            CorrectValid();
        }
        public ActorValue(double minVal, double maxVal, double curVal, double derivate) : this(minVal, maxVal, curVal)
        {
            this.derivate = derivate;
        }
        #endregion
    }
    [System.Serializable] public readonly ref struct ReadOnlyActorValue
    {
        private readonly double minValue, maxValue, value, derivate;

        #region props
        public double Derivate { get => derivate; }
        public double Value
        {
            get => value;
        }
        public double MaxValue
        {
            get => maxValue;
        }
        public double MinValue
        {
            get => minValue;
        }
        public double Range => MaxValue - MinValue;
        public double Percent => (Value - MinValue) / Range;

        public bool IsMin => value == minValue;
        public bool IsMax => value == maxValue;
        #endregion

        #region fabric
        private ReadOnlyActorValue(ActorValue readonlyReference)
        {
            minValue = readonlyReference.MinValue;
            maxValue = readonlyReference.MaxValue;
            value = readonlyReference.Value;
            derivate = readonlyReference.Derivate;
        }
        #endregion

        #region operators
        public static implicit operator ReadOnlyActorValue (ActorValue value)
        {
            return new ReadOnlyActorValue(value);
        }
        #endregion
    }
}

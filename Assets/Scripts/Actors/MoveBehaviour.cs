﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enviroment;
using Stopwatch = System.Diagnostics.Stopwatch;
using Control.Injectors;

namespace Actors
{
    public class MoveBehaviour : MonoBehaviour, IInputControl
    {
        #region constants
        public const float minSpeed = 1, maxSpeed = 100;
        public const string moveAxis = "Horizontal";
        public const string jumpAxis = "Jump";
        public const string ShiftAxis = "Fire3";
        #endregion

        #region fields
        [SerializeField] private float shiftSpeedMod = 1.5f, staminaOnShift = 10, jumpStaminaRequest = 15;
        [SerializeField] private bool internalInput = false;
        [SerializeField, Range(minSpeed, maxSpeed)] private float speedMove = 25;
        [SerializeField] private float maxAvalibleSpeedOnGround = 7, maxAvalibleSpeedOnAir = 30;
        [SerializeField] private float jumpHeight = 5;
        [SerializeField] private float smoothMove = 4, otherActorForce = 20;
        //init
        private Rigidbody2D rb;
        private PolygonCollider2D selfColliderBody;
        ContactFilter2D groundContactfilter;
        Transform meshbody;
        Stopwatch jumpWatch;
        StaminaBody stam;

        private bool isShifting = false;

        [Header("Cursor options")]
        public bool followToCursor = false;
        private Vector3 cursorPosInWorldSpace;
        [SerializeField] private float const_z = 0, deltaPixels = 40;
        public Vector3 CursorPositionInWorldSpace => cursorPosInWorldSpace;
        #endregion

        #region props
        public bool IsShifting => isShifting;
        /// <summary>
        /// Скорость, минимальное значение 1, максимальное значение 100
        /// </summary>
        public float SpeedMove
        {
            get => speedMove;
            set
            {
                //min val = 1
                if (value < minSpeed)
                    value = minSpeed;
                else if (value > maxSpeed)
                    value = maxSpeed;
                speedMove = value;
            }
        }

        public bool OnGround => onGround;
        public Vector2 Velocity => _rb_velocity;
        private Vector2 _rb_velocity;
        public Rigidbody2D AttachedRigidbody => rb;
        #endregion

        #region unity callback
        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            selfColliderBody = GetComponent<PolygonCollider2D>();
            if (rb is null)
                SendError();
            groundContactfilter = new ContactFilter2D()
            {
                layerMask = LayerMask.NameToLayer("Default"),
                useTriggers = false,
                useOutsideDepth = true,
            };
            meshbody = transform.Find("Meshbody");
            jumpWatch = Stopwatch.StartNew();
            impulseDelay = Stopwatch.StartNew();
            stam = GetComponent<StaminaBody>();
        }

        // Update is called once per frame
        //void FixedUpdate()
        //{
        //    //prev calc
        //    CalculateOnGround();

        //    //Move
        //    var moveDirection = Input.GetAxis(moveAxis);
        //    //if (moveDirection != 0)
        //    {
        //        //Debug.Log($"Direction is {moveDirection:f2}");
        //        MoveTo(new Vector2(moveDirection, 0));
        //    }

        //    //Jump
        //    var jumpButton = Input.GetAxis(jumpAxis);
        //    if (jumpButton != 0)
        //    {
        //        Jump();
        //    }

        //    //end
        //    Rotate();
        //    VelocityUpdate();
        //}

        void FixedUpdate()
        {
            CalculateOnGround();
            if(internalInput)
            {
                var input = this as IInputControl;
                input.OnInject(new Inject(moveAxis, 0));
                input.OnInject(new Inject(jumpAxis, 0));
            }
            Rotate();
            VelocityUpdate();
            _rb_velocity = rb.velocity;
        }

        private void SendError()
        {
            Debug.LogError($"Rigidbody2d component is missing");
            gameObject.SetActive(false);
        }
        #endregion

        #region IInpuControl
        void IInputControl.OnInject(Inject inject)
        {
            //prev calc
            //CalculateOnGround();

            if(InjectControl.TryGetAxis(inject, ShiftAxis, out var shiftDirection))
            {
                if (shiftDirection != 0)
                {
                    if (!stam.IsExhaustion)
                        isShifting = true;
                    else isShifting = false;
                }
                else isShifting = false;
            }

            //Move
            //var moveDirection = Input.GetAxis(moveAxis);
            if(InjectControl.TryGetAxis(inject, moveAxis, out var moveDirection))
            {
                //Debug.Log($"Direction is {moveDirection:f2}");
                MoveTo(new Vector2(moveDirection, 0));
            }

            //Jump
            //var jumpButton = Input.GetAxis(jumpAxis);
            if (InjectControl.TryGetAxis(inject, jumpAxis, out var jumpValue))
            {
                if(jumpValue!=0)
                   
                    Jump(moveDirection);
            }

            //end
            //Rotate();
            //VelocityUpdate();
        }
        #endregion

        #region behaviour
        private void MoveTo(Vector2 direction)
        {
            if (direction.sqrMagnitude != 0)
            {
                var shiftMod = 1f;
                if (isShifting && onGround)
                {
                    if (stam.RequestStamina(staminaOnShift * Time.fixedDeltaTime * rb.velocity.normalized.magnitude))
                    {
                        shiftMod = shiftSpeedMod;

                    }
                }

                var onGroundMod = onGround ? 1f : 0.1f;
                Vector2 delta = direction * speedMove * Time.fixedDeltaTime * rb.mass * onGroundMod * shiftMod;
                //Vector3 newPosition = new Vector3(delta.x, delta.y, 0);
                //rb.MovePosition(transform.position + (Vector3)delta);
                rb.AddForce(delta, ForceMode2D.Impulse);
                //Debug.Log($"Force is {delta} magnitude is {delta.magnitude}");
                var velocity = rb.velocity;
                float sign = velocity.x / Mathf.Abs(velocity.x);
                var maxAvailSpeed = onGround ? maxAvalibleSpeedOnGround : maxAvalibleSpeedOnAir;
               

                //rb.velocity = velocity;
                
                maxAvailSpeed *= shiftMod;

                if (Mathf.Abs(velocity.x) > maxAvailSpeed)
                    velocity.x = maxAvailSpeed * sign;
                rb.velocity = Vector2.Lerp(rb.velocity, velocity, Time.fixedDeltaTime * speedMove*0.6f * shiftMod);
            }
            else if(onGround)
            {
                var velocity = rb.velocity;
                velocity.x -= velocity.x * Time.deltaTime * smoothMove;
                if (Mathf.Abs(velocity.x) < 0.01f) velocity.x = 0;

                rb.velocity = velocity;
            }
        }

        //temp
        void Rotate()
        {
            //target
            var rightMove = Quaternion.Euler(0, -90, 0);
            var leftMove = Quaternion.Euler(0, 90, 0);
            var idle = Quaternion.Euler(0, 0, 0);

            if (followToCursor)
            {
                cursorPosInWorldSpace = PlayerAccess.Self.PlayerCamera.ScreenToWorldPoint(Input.mousePosition);
                cursorPosInWorldSpace.z = const_z;

                var delta = cursorPosInWorldSpace.x - transform.position.x;
                if(delta >= deltaPixels)
                {
                    meshbody.rotation = Quaternion.Lerp(meshbody.rotation, rightMove, Time.fixedDeltaTime * 4);
                }
                else if( delta <= -deltaPixels)
                {
                    meshbody.rotation = Quaternion.Lerp(meshbody.rotation, leftMove, Time.fixedDeltaTime * 4);
                }
                else
                {
                    meshbody.rotation = Quaternion.Lerp(meshbody.rotation, idle, Time.fixedDeltaTime * 4);
                }
            }
            else
            {
                if (rb.velocity.x > 0.1f)
                {
                    meshbody.rotation = Quaternion.Lerp(meshbody.rotation, rightMove, Time.fixedDeltaTime * 4);
                }
                else if (rb.velocity.x < 0.1f)
                {
                    meshbody.rotation = Quaternion.Lerp(meshbody.rotation, leftMove, Time.fixedDeltaTime * 4);
                }
                else
                {
                    meshbody.rotation = Quaternion.Lerp(meshbody.rotation, idle, Time.fixedDeltaTime * 4);
                }
            }
        }


        public void Jump(float xDirection)
        {
            if (jumpWatch.ElapsedMilliseconds > 500)
            {
                /*
                 * Removed to CalculateOnGround
                 */ 
                if (onGround)
                {
                    if (!stam.RequestStamina(jumpStaminaRequest))
                        return;
                    
                    Vector2 impulse = (new Vector2(xDirection, 1)).normalized * jumpHeight* rb.mass;

                    rb.AddForce(impulse, ForceMode2D.Impulse);
                    jumpWatch.Restart();
                }

            }
        }

        /*
         * New 
         */
        Collider2D[] cols = new Collider2D[8];
        int colsCount = 0;
        private bool onGround;
        private void CalculateOnGround()
        {
            colsCount = selfColliderBody.OverlapCollider(groundContactfilter, cols);
            onGround = false;
            for (int i = 0; i < colsCount; i++)
            {
                if (IsGround(GetAttributes(cols[i])))
                {
                    onGround = true;
                    break;
                }
            }

            bool IsGround(ObjectAttribute obj)
            {
                if (obj is null)
                    return false;
                //else if((obj.Get & Attributes.Ground) == Attributes.Ground)
                else if (obj.Get.HasFlag(Attributes.Ground))
                {
                    return true;
                }
                else return false;
            }
        }
        Stopwatch impulseDelay;
        private void VelocityUpdate()
        {
            if (impulseDelay.ElapsedMilliseconds > 100)
            {
                for (int i = 0; i < colsCount; i++)
                {
                    if(Impulse(GetAttributes(cols[i])))
                    {
                        impulseDelay.Restart();
                        return;
                    }
                }

                bool Impulse(ObjectAttribute @object)
                {
                    if (@object is null)
                        return false;

                    if (@object.Get.HasFlag(Attributes.Actor))
                    {
                        if (@object.gameObject != gameObject)
                        {
                            var direction = (transform.position - @object.transform.position).normalized; //rb.velocity.normalized
                            var curSpeed = rb.velocity.magnitude * 0.2f;
                            var freeForce = rb.mass * otherActorForce;
                            var force = freeForce > freeForce * curSpeed ? freeForce : freeForce * curSpeed;

                            rb.AddForce(direction * force, ForceMode2D.Impulse);
                            return true;
                        }
                    }

                    return false;
                }
            }
        }

        private ObjectAttribute GetAttributes(Collider2D col)
        {
            var obj = col.GetComponent<ObjectAttribute>();
            if (obj is null)
                obj = col.attachedRigidbody?.GetComponent<ObjectAttribute>();
            return obj;
        }
        #endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using Native;
using UnityEngine;
using UnityEngine.Jobs;

namespace Actors
{
    public class ProgressionManager : MonoBehaviour, IStatsDependet
    {
        #region fields
        [SerializeField]
        private string[] workedStats = new string[]
        {
            GenericValues.Stats.strength,
            GenericValues.Stats.agility,
            GenericValues.Stats.intelligence,
            GenericValues.Stats.constitution,
        };

        private const string stat_lvl = GenericValues.Stats.lvl;
        private const string stat_points = GenericValues.Stats.points;

        StatsBlock block;
        private ActorValue expValue;
        private double lvlInfo = 1;
        private double maxBaseExpToLvl = 40, modLvl = 2, expPow = 1.5;
        #endregion

        #region props
        public ReadOnlyActorValue Expirience => expValue;
        public double LvlInfo => lvlInfo;
        public double Points => block[stat_points].Value;
        #endregion

        #region stats support
        private static readonly string[] depStats = new string[]
        {
            //GenericValues.Stats.lvl,
            GenericValues.Stats.exp,
            //GenericValues.Stats.points,
        };

        IReadOnlyCollection<string> IStatsDependet.DependetStats => depStats;

        bool IStatsDependet.RequestSync => false;

        void IStatsDependet.OnChangeUpdate(StatsBlock.Stat[] dependetStats)
        {
            var exp = dependetStats[0]; //read only
            var lvl = exp.Root[stat_lvl];
            var points = exp.Root[stat_points];

            double lvlValue = 0, maxExp = 0, minExp = 0;

            do
            {
                lvlValue = lvl.Value;
                maxExp = maxBaseExpToLvl * (modLvl * System.Math.Pow(lvlValue, expPow) - 1d);
                minExp = maxBaseExpToLvl * (modLvl * System.Math.Pow(lvlValue - 1d, expPow) - 1d);
                if (minExp < 0) minExp = 0;

                expValue = new ActorValue(minExp, maxExp, exp.Value);

                if (expValue.IsMax)
                {
                    lvl.ChangeUnscaleValue(1d);
                    points.ChangeUnscaleValue(3d);
                }
                else break;
            }
            while (expValue.IsMax);

            lvlInfo = lvl.Value;
        }

        #endregion

        #region unity
        void Start()
        {
            block = GetComponent<StatsBlock>();
        }
        #endregion

        #region methods
        public void SetPointsInStat(double points, string statName)
        {
            bool isAvalible = false;
            foreach(var st in workedStats)
            {
                if(statName.Equals(st))
                {
                    isAvalible = true;
                    break;
                }
            }
            if (!isAvalible)
                return;

            var totalPoints = block[stat_points];
            if(totalPoints.Value < points)
            {
                points = totalPoints.Value;
            }

            totalPoints.ChangeUnscaleValue(-points);

            block[statName].ChangeAdditionalBase(points);
        }
        #endregion
    }
}
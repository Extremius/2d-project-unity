﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;
using Enviroment;
using Control.Injectors;

namespace Actors
{
    public class SimpleAttack : MonoBehaviour, IInputControl
    {
        public const string fire1 = "Fire2";
        [SerializeField] private GameObject spikes;
        [SerializeField] private double spikesLive = 5d, spikesReload = 10d;
        [SerializeField] private double staminaNeed;

        Stopwatch watcher = Stopwatch.StartNew(), inputWatcher = Stopwatch.StartNew();

        public Stopwatch Watcher => watcher;

        public double Reload => spikesReload;

        public bool SpikesActive => !spikesHiden;

        private bool spikesHiden = true;

        void Update()
        {
            if(!spikesHiden)
            {
                if(watcher.Elapsed.TotalSeconds > spikesLive)
                {
                    SetSpikes(false);
                    return;
                }
            }

        }

        void Start()
        {
            transform.Find("Spikes").Find("Colliders").GetComponent<Enemies.EnemySpikes>().OnKillHandler += OnKill;
            transform.Find("FootStamp").GetComponent<Enemies.EnemySpikes>().OnKillHandler += OnKill;
        }

        void OnKill(HealthBody body)
        {
            if (TryGetComponent<StatsBlock>(out var block))
            {
                //block[GenericValues.Stats.constitution].ChangeAdditionalBase(1);
                //block[GenericValues.Stats.constitution].ChangeAdditionalBase(1);
                if(body.TryGetComponent<StatsBlock>(out var killedBlock))
                {
                    var gainedExp =killedBlock[GenericValues.Stats.expOnKill].Value;
                    block[GenericValues.Stats.exp].ChangeUnscaleValue(gainedExp);
                }
            }
            else
                Debug.Log("Block is null");
        }

        void IInputControl.OnInject(Inject inject)
        {
            if(inject.type.Equals(fire1))
            {
                var isPressing = inject.AsBool;
                //Debug.Log($"Fire1 is pressing {isPressing}");
                if (isPressing)
                    ButtonPress();
            }
        }

        private void ButtonPress()
        {
            if (inputWatcher.ElapsedMilliseconds < 1000)
                return;
            inputWatcher.Restart();
            //Debug.Log("Fire1 is pressing");
            if(spikesHiden)
            {
                if(watcher.Elapsed.TotalSeconds>=spikesReload)
                {
                    if(TryGetComponent<StaminaBody>(out var stam))
                    {
                        if(stam.RequestStamina(staminaNeed))
                        {
                            SetSpikes(true);
                        }
                    }
                    else
                    {
                        SetSpikes(true);
                    }
                }
            }
            else
            {
                SetSpikes(false);
            }
        }

        private void SetSpikes(bool value)
        {
            if(value)
            {
                spikes.SetActive(true);
                spikesHiden = false;
                watcher.Restart();
            }
            else
            {
                spikes.SetActive(false);
                spikesHiden = true;
                watcher.Restart();
            }
        }
    }
}

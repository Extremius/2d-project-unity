﻿using Actors.UI;
using Native;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;


namespace Actors
{

    public class StaminaBody : MonoBehaviour, IStatsDependet, INativeComponent
    {
        #region version1
        ////private Stopwatch watcher; 

        //public const double coolDown = 1;

        //[SerializeField] private float staminaRecover = 1;
        //[SerializeField] private float staminaRecoverAcceleration = 1;
        //[SerializeField] private ActorValue _stamina;
        //[SerializeField] private int staminaLimitPercent = 20;
        //[SerializeField] private float absorptionDamagePercent = 30;


        //private Stopwatch watcher;
        //private float stm;


        //private void Start()
        //{
        //    watcher = Stopwatch.StartNew();

        //}


        //private float StaminaRec
        //{
        //    get
        //    {
        //        //if (staminaAccses.Value < staminaLimitPercent)
        //        //    return staminaRecover;
        //        //else
        //        return staminaRecover + stm;

        //    }

        //    set
        //    {
        //        stm += value;

        //        if (value == 0)
        //            stm = 0;

        //    }

        //}





        //protected ref ActorValue stamina
        //{
        //    get
        //    {
        //        return ref _stamina;
        //    }
        //}


        //public bool RequestStamina(double value)
        //{
        //    if (stamina.Value >= value)
        //    {
        //        stamina.Value -= value;
        //        return true;
        //    }
        //    else return false;
        //}



        //public ReadOnlyActorValue staminaAccses => stamina;

        //public double LimmitStamina => staminaAccses.Range * staminaLimitPercent * 0.01f;

        //public float AbsorptionDamage => absorptionDamagePercent * 0.01f;




        //public void ChangeStamina(double delta)
        //{


        //    if (delta < 0)
        //    {
        //        stamina.Value += delta;
        //        StaminaRec = 0;

        //        watcher = Stopwatch.StartNew();
        //    }



        //}



        //private void Update()
        //{

        //    if (watcher.Elapsed.TotalSeconds < coolDown)
        //        return;

        //    if (staminaAccses.Value > LimmitStamina)
        //        StaminaRec = staminaRecoverAcceleration * Time.deltaTime;
        //    else
        //        StaminaRec = 0;

        //    _stamina.Update((StaminaRec) * Time.deltaTime);



        //}


        //public void ResetStamina()
        //{
        //    _stamina.Value = _stamina.MaxValue;

        //}



        ////private void PassiveUpdate()
        ////{
        ////    if (watcher is null)
        ////        watcher = Stopwatch.StartNew();

        ////    var elapsedTime = watcher.Elapsed.TotalSeconds;
        ////    _stamina.Update(elapsedTime);

        ////    watcher.Restart();
        ////}


        #endregion

        #region version2
        [System.Serializable]private enum State
        {
            Idle,
            Regen,
            Exhaustion,
        }

        [SerializeField] private ActorValue _stamina;
        [SerializeField] private double absorbtionDmg = 0.3, minStaminaPercentOnAbsorbtion = 0.2;
        [SerializeField] private double staminaRegenDelay = 1, staminaFullRegenDelay = 4, staminaBaseRegen = 1, staminaRegenAcceleration = 4;

        [SerializeField] private State staminaState = State.Idle;
        Stopwatch watcher = Stopwatch.StartNew();

        #region props
        public bool IsExhaustion => staminaState == State.Exhaustion;
        protected ref ActorValue stamina => ref _stamina;
        public ReadOnlyActorValue StaminaAccess => _stamina;



        
        #endregion

        #region methods
        public bool RequestStamina(double value)
        {
            switch (staminaState)
            {
                case State.Exhaustion: return false;
                default:
                    {
                            ChangeValue(-value);
                            return true;
                    }

            }

        }
        public void SetStamina(double value)
        {
            _stamina.Value = value;
            if (_stamina.IsMin)
            {
                ChangeState(State.Exhaustion);
            }
            else if (_stamina.IsMax)
            {
                ChangeState(State.Idle);
            }
            else
            {
                ChangeState(State.Regen);
            }
            ResetStaminaRegen();
        }
            public double ChangeDamage(double damage)
        {
            if (damage >= 0)
                return 0;

            if (staminaState == State.Exhaustion)
                return damage;

            if (_stamina.Percent >= minStaminaPercentOnAbsorbtion)
            {
                ChangeValue(damage * absorbtionDmg);
                return damage * (1d - absorbtionDmg);
            }
            else
                return damage;
        }

        private void ChangeValue(double delta)
        {
            if (delta is 0)
                return;

            stamina.Value += delta;

            if(_stamina.IsMin)
            {
                ChangeState(State.Exhaustion);
            }
            else if(_stamina.IsMax)
            {
                ChangeState(State.Idle);
            }
            else
            {
                ChangeState(State.Regen);
            }

            if(delta<0)
            {
                ResetStaminaRegen();
            }
        }
        #endregion

        #region private
        private void ResetStaminaRegen()
        {
            switch(staminaState)
            {
                case State.Regen:
                case State.Exhaustion:
                    watcher.Restart();
                    _stamina.Derivate = staminaBaseRegen;
                    break;
            }
        }
        private bool ChangeState(State newState)
        {
            if (staminaState != newState)
            {
                staminaState = newState;
                return true;
            }
            else return false;
        }
        #endregion

        #region unity
        private void Start()
        {
            //Enviroment.PlayerAccess.Self.Thread.AddNative(this);
            //native = new NativeCode<StaminaBody>(Enviroment.PlayerAccess.Self.Thread, this);
            NativeHelper.AddNativeComponent(this);
        }
        #endregion

        #region INative support
        //private NativeCode<StaminaBody> native;
        
        void INative.Start()
        {

        }

        void INative.Update(NativeLoop loop)
        {
            if (_stamina.IsMax && (_stamina.Derivate >= 0))
                return;

            var regendDelayInSeconds = 0d;
            switch (staminaState)
            {
                case State.Regen:
                    regendDelayInSeconds = staminaRegenDelay;
                    break;
                case State.Exhaustion:
                    regendDelayInSeconds = staminaFullRegenDelay;
                    break;
            }

            if (watcher.Elapsed.TotalSeconds >= regendDelayInSeconds)
            {
                stamina.Update(loop.deltaTime);
                if (_stamina.IsMax)
                {
                    _stamina.Derivate = 0;
                    ChangeState(State.Idle);
                }
                else
                {
                    _stamina.Derivate += staminaRegenAcceleration * loop.deltaTime;
                }
            }
        }

        void INative.End(NativeLoop loop)
        {

        }
        #endregion

        #region IStatDependet support
        bool IStatsDependet.RequestSync => false;
        private static readonly string[] dependetStats = new string[]
            {
                GenericValues.Stats.constitution,
                GenericValues.Stats.addStamina,
                GenericValues.Stats.baseStamina,
                GenericValues.Stats.staminaPerCon,
            };

        IReadOnlyCollection<string> IStatsDependet.DependetStats => dependetStats;

        void IStatsDependet.OnChangeUpdate(StatsBlock.Stat[] stats)
        {
            var con = stats[0];
            var addStamina = stats[1];
            var baseStamina = stats[2];
            var staminahPerCon = stats[3];

            var newStaminaMax = (baseStamina.Value * (1 + (con.Value - con.Basic) * staminahPerCon.Value)) + addStamina.Value;
            if (_stamina.MaxValue != newStaminaMax)
                _stamina.MaxValue = newStaminaMax;

            if (!_stamina.IsMax && (staminaState == State.Idle))
                ChangeState(State.Regen);
        }
        #endregion
        #endregion
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;
using System.Threading;

namespace Actors
{
    public class StatsBlock : MonoBehaviour, IEnumerable<StatsBlock.Stat>
    {
        [SerializeField, TextArea(6, 12)] private string inputSource;
        private Dictionary<string, Stat> _stats;
        [SerializeField] private IStatsDependet[] dependetComponents;

        #region istat support
        void FindAllDependets()
        {
            dependetComponents = GetComponents<IStatsDependet>();

            foreach (var comp in dependetComponents)
            {
                if (comp.RequestSync)
                {
                    Stat[] requestStats = new Stat[comp.DependetStats.Count];
                    int iter = 0;
                    foreach (var stName in comp.DependetStats)
                    {
                        requestStats[iter++] = this[stName];
                    }

                    comp.OnChangeUpdate(requestStats);
                }
                else
                {
                    ChangeStatAsync(comp);
                }
            }
        }

        private List<Stat> updatedStats = new List<Stat>(8);
        private List<IStatsDependet> updatedComps = new List<IStatsDependet>(8);

        private void UpdateAllComps()
        {
            if (updatedStats.Count > 0)
            {
                foreach (var st in updatedStats)
                {
                    UpdSt(st);
                }

                //Debug
                Debug.Log($"Обновление {updatedComps.Count} компонентов\nОбновление {updatedStats.Count} статов");

                updatedStats.Clear();
                updatedComps.Clear();
            }
        }

        private void OnChangeStat(Stat changedStat)
        {
            if(!updatedStats.Contains(changedStat))
                updatedStats.Add(changedStat);
        }

        void UpdSt(Stat changedStat)
        {
            List<IStatsDependet> dependetComp = new List<IStatsDependet>(dependetComponents.Length);
            foreach (var comp in dependetComponents)
            {
                if (updatedComps.Contains(comp))
                    continue;

                foreach (var stName in comp.DependetStats)
                {
                    if (stName.Equals(changedStat.statName))
                    {
                        dependetComp.Add(comp);
                        updatedComps.Add(comp);
                        break;
                    }
                }
            }

            if (dependetComp.Count is 0)
                return;

            foreach (var comp in dependetComp)
            {
                if (comp.RequestSync)
                {
                    Stat[] requestStats = new Stat[comp.DependetStats.Count];
                    int iter = 0;
                    foreach (var stName in comp.DependetStats)
                    {
                        requestStats[iter++] = this[stName];
                    }

                    comp.OnChangeUpdate(requestStats);
                }
                else
                {
                    ChangeStatAsync(comp);
                }
            }
        }

        #region async
        private async void ChangeStatAsync(IStatsDependet comp)
        {
            await Task.Run(delegate {
                Stat[] requestStats = new Stat[comp.DependetStats.Count];
                int iter = 0;
                foreach (var stName in comp.DependetStats)
                {
                    requestStats[iter++] = this[stName];
                }

                comp.OnChangeUpdate(requestStats);
            });
            
        }
        #endregion
        #endregion

        #region access

        public Stat this[string name]
        {
            get
            {
                if (name.Equals(lastStatName))
                    if (lastStat != null)
                        return lastStat;

                if (_stats.TryGetValue(name, out var val))
                    return WriteLastStat(val);
                else
                {
                    return WriteLastStat(GenerateStat(name, 0));
                }
            }
        }
        public bool CreateNewStat(string name, double baseValue, out Stat newStat)
        {
            try
            {
                newStat = new Stat(this, name, baseValue);
                _stats.Add(name, newStat);
                newStat.OnChangeHandler += OnChangeStat;
                return true;
            }
            catch (System.ArgumentException)
            {
                newStat = null;
                return false;
            }
        }
        public bool DeleteStat(string name)
        {
            if (_stats.TryGetValue(name, out var stat))
            {
                if (lastStatName.Equals(name))
                {
                    lastStat = null;
                }

                stat.ClearRoot();
                return _stats.Remove(name);
            }
            else return false;
        }
        #endregion

        #region private
        private string lastStatName = string.Empty;
        private Stat lastStat;
        private Stat WriteLastStat(Stat statToWrite)
        {
            lastStatName = statToWrite.statName;
            return lastStat = statToWrite;
        }
        private Stat GenerateStat(string name, double value)
        {
            //Debug.Log($"{name} was created");
            try
            {
                var newStat = new Stat(this, name, value);
                _stats.Add(name, newStat);
                newStat.OnChangeHandler += OnChangeStat;
                return newStat;
            }
            catch (System.ArgumentException)
            {
                return _stats[name];
            }
        }
        #endregion

        #region IEnumerable support
        IEnumerator<Stat> IEnumerable<Stat>.GetEnumerator()
        {
            foreach (var pair in _stats)
            {
                yield return pair.Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var pair in _stats)
            {
                yield return pair.Value;
            }
        }
        #endregion

        #region unity
        private void Awake()
        {
            //_stats.Clear();
            ReadText(inputSource);
            lastStatName = string.Empty;
            lastStat = null;
        }

        void Start()
        {
            FindAllDependets();
        }

        void LateUpdate()
        {
            UpdateAllComps();
        }
        #endregion

        #region textReader
        private void ReadText(string text)
        {
            var textStats = text.Split(';');
            _stats = new Dictionary<string, Stat>(textStats.Length + 4);
            foreach (var element in textStats)
            {
                var textSt = element.Split(':');

                if (textSt.Length >= 2)
                {
                    if (double.TryParse(textSt[1], System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.InvariantInfo, out var val))
                    {
                        var statName = textSt[0].RemoveSpecialSymbols();
                        GenerateStat(statName, val);
                    }
                }
            }
        }

        #endregion

        #region stats
        public class Stat : IStatAccess
        {
            public readonly string statName;
            private ActorStat body;
            private StatsBlock root;
            public StatsBlock Root => root;

            public void ClearRoot()
            {
                root = null;
                OnChangeHandler = null;
            }

            #region delegating
            public event Action<Stat> OnChangeHandler;
            private Thread blockedThread;

            private void CallEvent()
            {
                blockedThread = Thread.CurrentThread;
                OnChangeHandler?.Invoke(this);
                blockedThread = null;
            }
            private void ThrowIfBlocked()
            {
                if (blockedThread != null)
                    if (Thread.CurrentThread == blockedThread)
                        throw new MethodAccessException("Невозможно вызвать метод, управление которого было получено от Ивента");
            }
            #endregion

            #region IStatAccess support
            public double Basic => body.baseValue;
            public double AdditionalBase => body.AdditionalBase;

            public bool IsClear => body.IsClear;

            public double ModificatorBase => body.ModificatorBase;

            public double UnscaleValue => body.UnscaleValue;

            public double Value => body.Value;

            public Stat(StatsBlock root, string name, double value)
            {
                this.root = root;
                statName = name;
                body = new ActorStat(value);
            }

            public void ChangeAdditionalBase(double changeValue)
            {
                ThrowIfBlocked();

                body.ChangeAdditionalBase(changeValue);

                CallEvent();
            }

            public void ChangeModificatorBase(double changeValue)
            {
                ThrowIfBlocked();

                body.ChangeModificatorBase(changeValue);

                CallEvent();
            }

            public void ChangeUnscaleValue(double changeValue)
            {
                ThrowIfBlocked();

                body.ChangeUnscaleValue(changeValue);

                CallEvent();
            }

            public void Reset()
            {
                ThrowIfBlocked();

                body.Reset();

                CallEvent();
            }

            #endregion
        }

        private struct ActorStat : IStatAccess
        {
            public readonly double baseValue;
            private double addValue, unscaleValue;
            private double mod;

            public double Value
            {
                get
                {
                    return (baseValue + addValue) * mod + unscaleValue;
                }
            }


            #region access
            public double Basic => baseValue;
            public double AdditionalBase
            {
                get => addValue;
                //set => addValue = value;  
            }
            public void ChangeAdditionalBase(double changeValue) => addValue += changeValue;
            public double ModificatorBase => mod;
            public void ChangeModificatorBase(double changeValue) => mod += changeValue;
            public double UnscaleValue => unscaleValue;
            public void ChangeUnscaleValue(double changeValue) => unscaleValue += changeValue;
            #endregion

            #region constructor
            public void Reset()
            {
                addValue = unscaleValue = 0;
                mod = 1;
            }
            public bool IsClear
            {
                get
                {
                    if (addValue is 0)
                        if (unscaleValue is 0)
                            if (mod is 1)
                                return true;
                    return false;
                }
            }

            public ActorStat(double value)
            {
                baseValue = value;
                addValue = unscaleValue = 0;
                mod = 1;
            }

            public ActorStat(double baseValue, double addValue, double mod, double unscaleValue)
            {
                this.baseValue = baseValue;
                this.addValue = addValue;
                this.unscaleValue = unscaleValue;
                this.mod = mod;
            }
            #endregion

            // (5+0)*1+0 = 5
            // (5+4)*1+0 = 9
            // (5+4)*1.2+0 = 10.8
            // (5+0)*1.2+4 = 10
        }

        private interface IStatAccess
        {
            double Basic { get; }
            double AdditionalBase { get; }
            bool IsClear { get; }
            double ModificatorBase { get; }
            double UnscaleValue { get; }
            double Value { get; }

            void ChangeAdditionalBase(double changeValue);
            void ChangeModificatorBase(double changeValue);
            void ChangeUnscaleValue(double changeValue);
            void Reset();
        }
        #endregion
    }

    public interface IStatsDependet
    {
        IReadOnlyCollection<string> DependetStats { get; }
        void OnChangeUpdate(StatsBlock.Stat[] dependetStats);

        bool RequestSync { get; }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Control;
using Control.Injectors;

namespace Actors.Weapons
{
    [System.Serializable]
    public sealed class AmmoHold
    {
        [SerializeField] private string name;
        [SerializeField] private int count;

        public string Name => name;
        public int Count
        {
            get => count;
            set
            {
                count = value;
            }
        }

        public AmmoHold(string name)
        {
            count = 0;
            this.name = name;
        }
    }
    public class WeaponHolder : MonoBehaviour, IInputControl
    {
        public const string fire1 = "Fire1";
        public const string reload = "Reload";
        #region fields
        [SerializeField] RawCursorPointer move;
        [SerializeField] LineRenderer line;
        [SerializeField] Transform shootTransform;
        [SerializeField] Rigidbody2D selfRb;
        [SerializeField] GameObject weaponToRotate;
        [SerializeField] GameObject weaponObject;
        [SerializeField] List<AmmoHold> allAmmos;
        IWeapon selfWeapon;

        public GameObject WeaponObject => weaponObject;
        Vector3[] linePos = new Vector3[2];
        Vector2 direction;
        #endregion

        #region props
        public Vector2 ShootPosition => shootTransform.position;
        public Vector2 Direction => direction;
        #endregion

        private void Start()
        {
            //move.followToCursor = true;
            CalcPositions();
            FillLine();

            selfWeapon = weaponObject.GetComponent<IWeapon>();
        }

        //RaycastHit2D[] hits = new RaycastHit2D[64];
        int totalHits = 0;
        void CalcPositions()
        {
            linePos[0] = shootTransform.position;
            //linePos[1] = move.CursorPositionInWorldSpace;
            direction = move.CursorPositionInWorld - shootTransform.position;
            direction.Normalize();

            //totalHits = Physics2D.RaycastNonAlloc(linePos[0], direction, hits, 1000);

            //for(int i=0;i<totalHits;i++)
            //{
            //    if (!hits[i].collider.isTrigger)
            //    {
            //        if (hits[i].rigidbody != selfRb)
            //        {
            //            linePos[1] = hits[i].point;
            //            return;
            //        }
            //    }
            //}

            //linePos[1] = move.CursorPositionInWorld;
            linePos[1] = (Vector2)shootTransform.position + direction * 5;
        }

        void FillLine() => line.SetPositions(linePos);

        private void FixedUpdate()
        {
            CalcPositions();
            FillLine();
            RotateWeapon();
        }

        void RotateWeapon()
        {
            var dir = (Vector2)move.CursorPositionInWorld - (Vector2)weaponToRotate.transform.position;
            dir.Normalize();
            dir *= -1;
            var targetRotation = Quaternion.LookRotation(dir, Vector3.up);
            weaponToRotate.transform.rotation = Quaternion.Lerp(weaponToRotate.transform.rotation, targetRotation, Time.fixedDeltaTime * 8);
        }

        void IInputControl.OnInject(Inject inject)
        {
            if (inject.type.Equals(fire1))
            {
                var isPressing = inject.AsBool;
                //Debug.Log($"Fire1 is pressing {isPressing}");
                if (isPressing)
                    ButtonPress();
            }

            if (inject.type.Equals(reload))
            {
                var isPressing = inject.AsBool;
                //Debug.Log($"Fire1 is pressing {isPressing}");
                if (isPressing)
                    selfWeapon.Reload();
            }
        }

        void ButtonPress()
        {
            selfWeapon?.Fire();
        }
        public AmmoHold GetAmmo(string name)
        {
            foreach(var am in allAmmos)
            {
                if(name.Equals(am.Name))
                {
                    return am;
                }
            }

            var newAm = new AmmoHold(name);
            allAmmos.Add(newAm);
            return newAm;
        }
        public void OnKill(HealthBody body)
        {
            if (TryGetComponent<StatsBlock>(out var block))
            {
                //block[GenericValues.Stats.constitution].ChangeAdditionalBase(1);
                //block[GenericValues.Stats.constitution].ChangeAdditionalBase(1);
                if (body.TryGetComponent<StatsBlock>(out var killedBlock))
                {
                    var gainedExp = killedBlock[GenericValues.Stats.expOnKill].Value;
                    block[GenericValues.Stats.exp].ChangeUnscaleValue(gainedExp);
                }
            }
            else
                Debug.Log("Block is null");
        }
    }
    
    public interface IWeapon
    {
        void Fire();
        void Reload();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Control.Injectors
{
    public class PlayerInjector : MonoBehaviour
    {
        #region fields
        [SerializeField] private IInputControl[] moveController;
        [SerializeField] List<string> axisNames; //unity init
        private IternalInjector inputManager;
        #endregion

        #region unity callback
        private void Start()
        {
            inputManager = new IternalInjector(this);
            moveController = GetComponents<IInputControl>();
            if (moveController == null)
                throw new NullReferenceException($"{nameof(moveController)} is null");
        }

        private void FixedUpdate()
        {
            foreach(var axis in axisNames)
            {
                var val = Input.GetAxis(axis);
                inputManager.Inject(new Inject(axis, val));
            }
        }
        #endregion

        private sealed class IternalInjector : InjectorManager
        {
            public readonly PlayerInjector root;

            public IternalInjector(PlayerInjector root) 
            {
                this.root = root;
                buffer = null;
            }

            #region overrides
            public override void Inject(Inject newInject)
            {
                foreach(var input in root.moveController)
                    input.OnInject(newInject);
            }

            public override bool GetInject(out Inject inject)
            {
                throw new NotSupportedException("Метод на данном экземпляре не поддерживается");
            }
            #endregion
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Control
{
    public class CameraMove : MonoBehaviour
    {
        #region fields
        [SerializeField] private Transform target;
        [SerializeField] private Vector2 offset;
        [SerializeField] private float interpolateSpeed = 8;
        [SerializeField] private Vector2 velocityMod = new Vector2(0.2f, 0.6f);
        [SerializeField, Header("FOV options")] 
        private bool enableDynamicFov = true;
        [SerializeField] private Camera selfCam;
        [SerializeField, Tooltip("X is min fov\nY is max fov")] private Vector2 fov = new Vector2(5, 15); //x is min; y is max
        [SerializeField] private float maxSpeedForFov = 30, fovChangeSpeed=3;
        //private
        Rigidbody2D targetRB;
        Vector2 trueOffset;
        #endregion

        private void Start()
        {
            targetRB = target?.GetComponent<Rigidbody2D>();

            if (selfCam is null)
                selfCam = GetComponent<Camera>();
        }

        private void FixedUpdate()
        {
            //calc offset
            trueOffset = offset;
            var velocity = targetRB.velocity;
            trueOffset += velocity * velocityMod;

            //move
            var curPos = new Vector3(target.position.x + trueOffset.x, target.position.y + trueOffset.y, transform.position.z);
            //transform.position = curPos;
            transform.position = Vector3.Lerp(transform.position, curPos, Time.deltaTime * interpolateSpeed);

            //cam fov
            if((selfCam!=null) && enableDynamicFov)
            {
                //interpolate
                var currentSpeed = targetRB.velocity.magnitude;
                var orientedSpeedPercent = currentSpeed / maxSpeedForFov;
                var fovRange = fov.y - fov.x;
                var absoluteFov = fovRange * orientedSpeedPercent + fov.x;

                var deltaFov = Mathf.Lerp(selfCam.orthographicSize, absoluteFov, fovChangeSpeed * Time.fixedDeltaTime);
                selfCam.orthographicSize = deltaFov;
            }
            else if(!enableDynamicFov && (selfCam!=null))
            {
                if(selfCam.orthographicSize != fov.x)
                selfCam.orthographicSize = fov.x;
            }
        }
    }
}

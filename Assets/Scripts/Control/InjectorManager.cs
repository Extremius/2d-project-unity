﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Injectors
{
    public abstract class InjectorManager
    {
        #region protected fields
        protected LinkedList<Inject> buffer;
        public int injectionCount => buffer.Count;
        #endregion

        #region abstract methods
        public virtual void Inject(Inject newInject)
        {
            buffer.AddLast(newInject);
        }
        public virtual bool GetInject(out Inject inject)
        {
            if(buffer.Count>0)
            {
                var node = buffer.First;
                buffer.Remove(node);
                inject = node.Value;

                return true;
            }
            else
            {
                inject = default;
                return false;
            }
        }
        #endregion

        #region factory
        protected InjectorManager()
        {
            buffer = new LinkedList<Inject>();
        }
        #endregion

    }
    public readonly struct Inject
    {
        public string type { get; }
        private float value { get; }

        #region methods
        public bool AsBool => Math.Abs(value) < 0.5f ? false : true;
        public float AsSingle => value;
        public int AsInt32 => (int)value;
        #endregion

        public Inject(string injectType, float injectValue)
        {
            type = injectType;
            value = injectValue;
        }
    }

    public static class InjectControl
    {
        public static float GetAxis(Inject inject, string axisName)
        {
            if (inject.type.Equals(axisName))
                return inject.AsSingle;
            else return 0;
        }

        public static bool TryGetAxis(Inject inject, string axisName, out float value)
        {
            if (inject.type.Equals(axisName))
            {
                value = inject.AsSingle;
                return true;
            }
            else
            {
                value = 0;
                return false;
            }
        }
    }

    public interface IInputControl
    {
        void OnInject(Inject inject);
    }
}

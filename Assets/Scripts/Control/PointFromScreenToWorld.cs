﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointFromScreenToWorld : MonoBehaviour
{
    [SerializeField] Camera mainCam;
    [SerializeField] GameObject obj;
    [SerializeField] float constant_z = 0;

    private void Start()
    {
        Application.targetFrameRate = 15;
    }
    private void Update()
    {
        Vector3 screenMousePos = Input.mousePosition;
        var globalPosition = mainCam.ScreenToWorldPoint(screenMousePos);
        globalPosition.z = constant_z;

        obj.transform.position = globalPosition;
    }
}

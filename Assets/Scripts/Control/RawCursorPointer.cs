﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors.AI;
using Enviroment;

namespace Control
{
    public class RawCursorPointer : MonoBehaviour
    {
        public const string pointer_namespace = "private:cursor";
        public const string pointer_position = "position";
        public Vector3 CursorPositionInWorld => cursorPos;
        [SerializeField] private float const_z = 0;
        private Vector3 cursorPos;

        [SerializeField] MemoryModule memory;
        

        private void Update()
        {
            if(memory == null)
            {
                //player
                var pos = PlayerAccess.Self.PlayerCamera.ScreenToWorldPoint(Input.mousePosition);
                pos.z = const_z;
                cursorPos = pos;
            }
            else
            {

                //ai
                if (memory.ReadBlock(pointer_namespace, out var memBlock))
                {
                    if (memBlock.Read<Vector3>(pointer_position, out var cursorPos))
                    {
                        cursorPos.z = const_z;
                        this.cursorPos = cursorPos;
                    }
                
                }
            }
        }
    }
}

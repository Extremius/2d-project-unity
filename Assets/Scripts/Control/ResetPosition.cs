﻿using Actors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    #region fields
    [SerializeField] private Transform target, resetPosition;
    #endregion
    private void Start()
    {
        Application.targetFrameRate = 60;
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            if (target != null)
            {
                ResetSelf();
            }
        }
    }

    public void ResetSelf()
    {
        target.position = resetPosition.position;
        target.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        target.GetComponent<Actors.HealthBody>()?.ResetHealth();
        //target.GetComponent<Actors.ActorBody>()?.SetArmor(0.3*;
        target.GetComponent<StaminaBody>()?.SetStamina(99999);

        Debug.Log($"{target.name} position reset");
    }
}

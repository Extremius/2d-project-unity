﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Actors.Editor
{
    [CustomEditor(typeof(StatsBlock))]
    public class StatsBlockEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("See all stats"))
            {
                var obj = target as StatsBlock;

                PrintAllStats(obj);

            }
        }

        #region behaviour
        private void PrintAllStats(StatsBlock block)
        {
            if (block == null)
            {
                Debug.Log($"Object is null");
                return;
            }

            string output = $"{block.name}";
            foreach (var stat in block)
            {
                output += $"\n{stat.statName} = {stat.Value:f1}";
            }

            Debug.Log(output);
        }
        #endregion
    }
}

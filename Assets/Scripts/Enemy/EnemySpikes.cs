﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enviroment;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace Actors.Enemies
{
    public class EnemySpikes : MonoBehaviour
    {
        #region fields
        [SerializeField] private double touchDamage = 20;
        [SerializeField] private float forceStrength = 20;
        [SerializeField] private Attributes attributes = Attributes.Actor;

        private Rigidbody2D rb;
        private Collider2D internalCol;
        ContactFilter2D overlapFilter;

        public event System.Action<HealthBody> OnKillHandler;
       
        #endregion

        #region unity callback
        private void Start()
        {
            rb = GetComponentInParent<Rigidbody2D>();
            internalCol = GetComponent<Collider2D>();
            overlapFilter = new ContactFilter2D()
            {
                layerMask = LayerMask.NameToLayer("Default"),
                useTriggers = false,
                useOutsideDepth = true,
            };
        }

        Collider2D[] cols = new Collider2D[6];
        private void FixedUpdate()
        {
            var colCount = internalCol.OverlapCollider(overlapFilter,cols);
            for(int i =0;i<colCount;i++)
            {
                DoAction(GetAttributes(cols[i]));
            }
        }
        #endregion

        #region private
        private void DoAction(ObjectAttribute attribute)
        {
            if (attribute is null)
                return;

            if (attribute.gameObject == gameObject)
                return;

            if (transform.root.gameObject == attribute.gameObject)
                return;

            if(attribute.Get.HasFlag(attributes))
            {
                if(attribute.TryGetComponent<HealthBody>(out var healthBody))
                {
                    healthBody.OnDieHandler += OnKillBody;
                    healthBody.ChangeHealth(transform.root.gameObject, -touchDamage);
                    healthBody.OnDieHandler -= OnKillBody;

                    if (attribute.TryGetComponent<Rigidbody2D>(out var targetRb))
                    {
                        var direction = attribute.transform.position - transform.position;

                        targetRb.AddForce(direction.normalized * forceStrength, ForceMode2D.Impulse);
                    }
                }
            }
        }

        void OnKillBody(HealthBody body, GameObject source)
        {
            if (source == transform.root.gameObject)
            {
                OnKillHandler?.Invoke(body);
                Debug.Log("Invoked OnKillHandler");
            }
        }

        private ObjectAttribute GetAttributes(Collider2D col)
        {
            var obj = col.GetComponent<ObjectAttribute>();
            if (obj is null)
                obj = col.attachedRigidbody?.GetComponent<ObjectAttribute>();
            return obj;
        }
        #endregion
    }
}

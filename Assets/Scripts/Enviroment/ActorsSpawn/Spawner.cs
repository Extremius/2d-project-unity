﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace Enviroment
{
    public class Spawner : MonoBehaviour
    {
        #region fields
        [SerializeField] private GameObject ObjectPrefab;
        [SerializeField] private Transform[] spawnRndPositions;
        [SerializeField] private GameObject[] allSpawnedObjects;
        [SerializeField, Tooltip("X - is min\nY - is max")] private Vector2 spawnDelay;

        //private
        private int totalCount = 0;
        Stopwatch spawnTime;
        private float time;
        #endregion

        #region unity callbakcs
        private void Start()
        {
            spawnTime = Stopwatch.StartNew();
            //UpdateSpawnDelay();
            time = 0;
        }
        private void Update()
        {
            if (spawnTime.Elapsed.TotalSeconds > time)
            {
                SpawnObject();
                UpdateSpawnDelay();
            }

        }
        #endregion

        #region methods
        private void SpawnObject()
        {
            if (ObjectPrefab == null)
                return;
            

            UpdateList();
            if (totalCount >= allSpawnedObjects.Length)
                return;

            var newObject = Instantiate(ObjectPrefab);
            int rndPos = Random.Range(0, spawnRndPositions.Length);
            newObject.transform.position = spawnRndPositions[rndPos].position;
            //allSpawnedObjects.Add(newObject);
            allSpawnedObjects[totalCount++] = newObject;
        }

        private void UpdateList()
        {
            if (totalCount < 1)
                return;

            List<int> emptyID = new List<int>(allSpawnedObjects.Length);
            for(int i=0;i<totalCount;i++)
            {
                if(allSpawnedObjects[i] == null)
                {
                    emptyID.Add(i);
                }
            }
            if(emptyID.Count == allSpawnedObjects.Length)
            {
                totalCount = 0;
                return;
            }

            if (emptyID.Count > 0)
            {
                int lastEmptyID = emptyID[0];
                int totalEmptyCellsCount = 0;
                int emptyIterator = 0;
                for (int i = emptyID[emptyIterator++]; i < allSpawnedObjects.Length; i++)
                {
                    if((totalEmptyCellsCount >0) && (allSpawnedObjects[i] != null))
                    {
                        allSpawnedObjects[i - totalEmptyCellsCount] = allSpawnedObjects[i];
                        allSpawnedObjects[i] = null;
                        continue;
                    }
                    if(i == lastEmptyID)
                    {
                        totalEmptyCellsCount += 1;
                        if(emptyIterator < emptyID.Count)
                        lastEmptyID = emptyID[emptyIterator++];
                        continue;
                    }
                    
                }
            }

            totalCount = 0;
            foreach(var obj in allSpawnedObjects)
            {
                if (obj != null)
                    totalCount++;
            }
        }

        private void UpdateSpawnDelay() { time = Random.Range(spawnDelay.x, spawnDelay.y); spawnTime.Restart(); }
        #endregion
    }
}

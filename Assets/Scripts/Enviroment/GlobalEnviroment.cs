﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class GlobalEnviroment : MonoBehaviour
    {
        private static GlobalEnviroment _self;
        public static GlobalEnviroment Self => _self;

        private void Awake()
        {
            if (_self == null)
            {
                _self = this;
                SelfInit();
            }
            else if (_self != this)
                Destroy(gameObject);
        }

        void SelfInit()
        {

        }

        #region fields
        [SerializeField] PoolSupport.PoolManager poolManager;
        public PoolSupport.PoolManager PoolManager => poolManager;
        #endregion
    }
}

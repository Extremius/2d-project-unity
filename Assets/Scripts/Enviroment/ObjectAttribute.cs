﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    [System.Flags]
    public enum Attributes
    {
        None = 0b_0000_0000,
        Ground = 0b_0000_0001,
        Physic_Object = 0b_0000_0010,
        Actor = 0b_0000_0100,
        Pickup_Object = 0b_0000_1000,
        Player = 0b_0001_0000,
        Enemy = 0b_0010_0000,
    }
    public class ObjectAttribute : MonoBehaviour
    {
        [SerializeField] private Attributes objectAttributes = Attributes.None;

        public Attributes Get => objectAttributes;
    }
}

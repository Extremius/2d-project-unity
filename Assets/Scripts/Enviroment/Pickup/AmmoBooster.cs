﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment.Objects
{
    public class AmmoBooster : PickupObject
    {
        [SerializeField] List<Data> ammoBoost;
        protected override void PickupTo(GameObject obj)
        {
            if (obj.TryGetComponent<Actors.Weapons.WeaponHolder>(out var holder))
            {
                foreach (var dt in ammoBoost)
                {
                    var ammo = holder.GetAmmo(dt.name);
                    ammo.Count += dt.count;
                }
            }
        }
        [System.Serializable]
        private struct Data
        {
            public string name;
            public int count;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment.Objects
{
    public class HealthBooster : PickupObject
    {
        [SerializeField] double healthBoost;
        [SerializeField] double armorBoost;
        protected override void PickupTo(GameObject obj)
        {
            if(obj.TryGetComponent<Actors.ActorBody>(out var body))
            {
                body.ChangeHealth(null, healthBoost);
                body.ChangeArmor(armorBoost);
            }
        }
    }
}

﻿using Actors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment.Objects
{
    public abstract class PickupObject : MonoBehaviour
    {
        private bool isUp = false;
        private void OnTriggerEnter2D (Collider2D collision)
        {
            if (isUp)
                return;
            if (collision.isTrigger)
                return;

            if(collision.attachedRigidbody != null)
            {
                if (collision.attachedRigidbody.TryGetComponent<ObjectAttribute>(out var body))
                {
                    if (body.Get.HasFlag(Attributes.Actor))
                    {
                        isUp = true;
                        PickupTo(body.gameObject);
                        SelfDestroy();
                    }
                }
            }
        }

        protected abstract void PickupTo(GameObject obj);
        
        protected virtual void SelfDestroy()
        {
            GetComponent<Collider2D>().enabled = false;
            var rb = GetComponent<Rigidbody2D>();
            rb.bodyType = RigidbodyType2D.Dynamic;
            Destroy(gameObject, 3f);
        }

    }
}

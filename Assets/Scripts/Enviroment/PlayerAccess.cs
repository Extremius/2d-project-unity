﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Native;

namespace Enviroment
{
    public class PlayerAccess : MonoBehaviour
    {
        private static PlayerAccess _self;
        public static PlayerAccess Self => _self;

        private void Awake()
        {
            if(_self == null)
            {
                _self = this;
                Init();
            }
            else if(_self!=this)
            {
                Destroy(gameObject);
            }
        }

        void Init()
        {
            //nativeThread = new NativeThread(new List<INative>(8));
            //nativeThread.UpdatesPerSecond = 50;
            //nativeThread.Execute();
        }

        #region fields
        [SerializeField] private ResetPosition resetPositionScript;
        [SerializeField] private Camera playerCam;
        //private NativeThread nativeThread;

        
        #endregion

        #region props
        public ResetPosition ResetPositionScript => resetPositionScript;
        public Camera PlayerCamera => playerCam;
       
        //public NativeThread Thread => nativeThread;
        #endregion
    }
}

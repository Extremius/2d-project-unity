﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Enviroment.PoolSupport
{
    public sealed class Pool<T> : IPool where T:UnityEngine.Component
    {
        const string generator = "generator";

        int baseCapacity;
        T baseObject;
        List<T> allObjects;
        UnityPool unityPool;

        public T BaseComponent => BaseComponent;
        public Transform Parent => unityPool.transform;
        public UnityPool BasePool => unityPool;

        private T GetObject()
        {
            foreach(var obj in allObjects)
            {
                if (!obj.gameObject.activeSelf)
                    return obj;
            }

            //TODO: add reinit

            var newObj = LocalInstanceNewCopy();

            BasePool.StartCoroutine(GenerateNewObjects(baseCapacity-1), generator);

            return newObj;
        }

        public T Object() => GetObject();

        #region routines
        private T LocalInstanceNewCopy()
        {
            var copy = UnityEngine.Object.Instantiate<T>(baseObject, Vector3.zero, Quaternion.identity, Parent);
            allObjects.Add(copy);
            copy.gameObject.SetActive(false);

            return copy;
        }

        IEnumerator GenerateNewObjects(int count)
        {
            for(int i=0;i<count;i++)
            {
                LocalInstanceNewCopy();
                //Debug.Log($"Generate new object, left {count - i}");
                yield return new WaitForEndOfFrame();
            }

            BasePool.StopCoroutine(generator);
        }
        #endregion

        #region IPool support
        Component IPool.GetObject()
        {
            return GetObject() as Component;
        }

        T1 IPool.GetObject<T1>()
        {
            return GetObject() as T1;
        }

        bool IPool.EqualsType(Component component)
        {
            //return typeof(T).IsEquivalentTo(component.GetType());
            return baseObject == component;
        }
        #endregion

        public Pool(T baseObject, int baseCapacity, UnityPool basePool)
        {
            this.baseObject = baseObject;
            allObjects = new List<T>(baseCapacity);
            this.unityPool = basePool;
            this.baseCapacity = baseCapacity;

            LocalInstanceNewCopy();
            BasePool.StartCoroutine(GenerateNewObjects(baseCapacity-1), "generator");
        }
            

    }

    public interface IPool
    {
        Component GetObject();
        T GetObject<T>() where T : UnityEngine.Component;
        bool EqualsType(UnityEngine.Component component);
    }
}

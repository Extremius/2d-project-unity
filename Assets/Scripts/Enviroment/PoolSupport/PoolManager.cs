﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment.PoolSupport
{
    public class PoolManager : MonoBehaviour
    {
        [SerializeField] int baseCapacity = 16;

        List<IPool> allPools = new List<IPool>(8);

        #region manage
        public Pool<T> GetPool<T>(T baseComponent) where T : Component
        {
            foreach(var pol in allPools)
            {
                if(pol.EqualsType(baseComponent))
                {
                    return pol as Pool<T>;
                }
            }

            return GenerateNewPool(baseComponent);
        }

        private Pool<T> GenerateNewPool<T>(T baseComponent) where T : Component
        {
            var newTransform = new GameObject(baseComponent.name).transform;
            newTransform.SetParent(transform);
            newTransform.position = Vector3.zero;
            var basePool = newTransform.gameObject.AddComponent<UnityPool>();

            IPool newPool = new Pool<T>(baseComponent, baseCapacity, basePool);
            allPools.Add(newPool);

            return newPool as Pool<T>;
        }
        #endregion
    }
}

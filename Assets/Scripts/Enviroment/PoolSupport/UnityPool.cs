﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Enviroment.PoolSupport
{
    public class UnityPool : MonoBehaviour
    {
        private Dictionary<string, Coroutine> allCoroutines = new Dictionary<string, Coroutine>(4);
        public void StartCoroutine(IEnumerator method, string name)
        {
            if (!allCoroutines.ContainsKey(name))
            {
                var coroutine = base.StartCoroutine(method);
                allCoroutines.Add(name, coroutine);
            }
        }
        public new void StopCoroutine(string name)
        {
            if(allCoroutines.TryGetValue(name, out var coroutine))
            {
                if (coroutine != null)
                    base.StopCoroutine(coroutine);

                allCoroutines.Remove(name);
            }
        }


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class StringUtil
{
    public static string RemoveSpecialSymbols(this string self)
    {
        for(int i =0;i<self.Length;i++)
        {
            switch(self[i])
            {
                case '\n':
                case '\t':
                case '\0':
                case '\a':
                case '\b':
                case '\r':
                case '\f':
                case '\v':
                    continue;
                default:
                    return self.Remove(0, i);
            }
        }

        return string.Empty;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Native
{
    public class NativeDelegation : MonoBehaviour
    {
        private int castCountInFrame = 4;

        private LinkedList<Action> inCoroutineDelegating = new LinkedList<Action>();
        private LinkedList<Action> fastDelegating = new LinkedList<Action>();

        private readonly object _sync = new object();

        #region methods
        private void Start()
        {
            StartCoroutine(CastCoroutine());
        }

        IEnumerator CastCoroutine()
        {
            while(true)
            {
                var count = inCoroutineDelegating.Count;
                if (count > 0)
                {
                    count = count > castCountInFrame ? castCountInFrame : count;
                    for (int i = 0; i < count; i++)
                    {
                        var del = inCoroutineDelegating.First;
                        Cast(del.Value);
                        inCoroutineDelegating.Remove(del);
                    }
                }
                yield return new WaitForEndOfFrame();
            }
        }

        void Cast(Action action)
        {
            try
            {
                action?.Invoke();
            }
            catch(System.Exception e)
            {
                Debug.LogError(e);
            }
        }

        private void Update()
        {
            if(fastDelegating.Count>0)
            {
                lock(_sync)
                {
                    foreach(var node in fastDelegating)
                    {
                        Cast(node);
                    }
                    fastDelegating.Clear();
                }
            }
        }

        /// <summary>
        /// Делегирует вызов метода основному потоку, таким образом поддерживая вызов Unity API. Не возвращает значений
        /// </summary>
        /// <param name="action"></param>
        /// <param name="force"></param>
        public void Delegate(Action action, bool force)
        {
            if (action is null)
                return;

            if(force)
            {
                lock(_sync)
                {
                    fastDelegating.AddLast(action);
                }
            }
            else
            {
                inCoroutineDelegating.AddLast(action);
            }
        }
        #endregion
    }


}

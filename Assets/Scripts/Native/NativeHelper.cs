﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Native
{
    public class NativeHelper : MonoBehaviour, IDisposable
    {
        private static NativeHelper _self;
        //public static NativeHelper Self => _self;

        private void Awake()
        {
            if (_self == null)
            {
                _self = this;
                Init();
            }
            else if (_self != this)
                Destroy(this);
        }

        void Init()
        {
            componentsThread = new NativeThread(new List<INative>(8));
            componentsThread.UpdatesPerSecond = 50;

            aiThread = new NativeThread(new List<INative>(16));
            aiThread.UpdatesPerSecond = 20;

            mainThread = gameObject.AddComponent<NativeDelegation>();

            componentsThread.Execute();
            aiThread.Execute();
        }

        #region fields
        private NativeThread componentsThread, aiThread;
        private NativeDelegation mainThread;

        protected static NativeThread Components => _self.componentsThread;
        protected static NativeThread AILoop => _self.aiThread;
        public static NativeDelegation MainThread => _self.mainThread;
        #endregion


        public static void AddNativeComponent(INativeComponent comp)
        {
            Components.AddNative(comp);
        }

        public static void AddNativeAI(INativeAILoop ai)
        {
            AILoop.AddNative(ai);
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.

                componentsThread.Dispose();
                aiThread.Dispose();
                componentsThread = null;
                aiThread = null;
                _self = null;
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        ~NativeHelper()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(false);
        }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            GC.SuppressFinalize(this);
        }
        #endregion

        #region unity
        private void OnApplicationPause(bool pause)
        {
            componentsThread.Enable = !pause;
            aiThread.Enable = !pause;
        }
        void OnApplicationQuit()
        {
            Dispose(true);
        }
        #endregion
    }

    #region Special Interfaces
    public interface INativeComponent : INative
    {

    }
    public interface INativeAILoop : INative
    {

    }
    #endregion
}

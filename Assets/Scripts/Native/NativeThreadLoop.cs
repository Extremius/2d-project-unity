﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace Native
{
    public class NativeThread : IDisposable, IExecutable
    {
        private readonly object _nativeLock = new object();
        private ICollection<INative> allNativeObjects;
        private Thread mainThread;
        private Stopwatch time;
        private bool isRunning = false;
        private ulong loopsCount =0;

        #region factory
        public NativeThread(ICollection<INative> natives)
        {
            allNativeObjects = natives;
        }
        #endregion

        #region methods
        public void AddNative(INative nativeObject)
        {
            if (nativeObject is null)
                return;
            //UnityEngine.Debug.Log("Start inser new native interface");
            lock (_nativeLock)
            {
                if (!allNativeObjects.Contains(nativeObject))
                {
                    nativeObject.Start();
                    allNativeObjects.Add(nativeObject);
                    //UnityEngine.Debug.Log("End inser new native interface");
                }
            }
        }
        private bool isEnabled = false;
        public bool Enable { get => isEnabled; set => isEnabled = value; }
        public bool RemoveNative(INative nativeObject)
        {
            if (nativeObject is null)
                return false;

            lock(_nativeLock)
            {
                if(allNativeObjects.Contains(nativeObject))
                {
                    var timeNow = DateTime.UtcNow;
                    var deltaTime = timeNow - lastCall;

                    var loop = new NativeLoop(deltaTime.TotalSeconds, loopsCount);

                    nativeObject.End(loop);

                    return allNativeObjects.Remove(nativeObject);
                }
                return false;
            }
        }
        #endregion

        #region props
        public int TotalNativesCount => allNativeObjects.Count;
        public double LifeTime => time.Elapsed.TotalSeconds;
        public ulong LoopsCount => loopsCount;
        public bool IsRunning => isRunning;
        private long ticksDelay;
        public double UpdatesPerSecond
        {
            get => 1d / (ticksDelay * 0.0000001d);

            set
            {
                if (value < 1)
                    return;

                ticksDelay = (long)((1d / value) * 10_000_000);
            }
        }
        #endregion

        #region behaviour
        public void Execute()
        {
            time = Stopwatch.StartNew();
            CallStart();

            mainThread = new Thread(Loop);
            mainThread.Name = "Native Thread";
            isRunning = true;
            isEnabled = true;
            lastCall = DateTime.UtcNow;
            mainThread.Start();
        }
        public void Terminate()
        {
            if(!isRunning)
            {
                throw new InvalidOperationException("Невозможно остановить не рабочий поток");
            }
            isEnabled = true;
            isRunning = false;
            time.Stop();
            mainThread.Join(5000);
        }
        private void CallStart()
        {
            foreach(var obj in allNativeObjects)
            {
                if(obj != null)
                {
                    try
                    {
                        obj.Start();
                    }
                    catch(Exception e)
                    {
                        UnityEngine.Debug.LogError(e);
                    }
                }
            }
        }
        private void Loop()
        {
            while (isRunning)
            {
                Thread.Sleep(new TimeSpan(ticksDelay));
                var loop = GenerateNativeloop();
                if (isEnabled)
                {
                    lock (_nativeLock)
                    {
                        foreach (var obj in allNativeObjects)
                        {
                            TryCallUpdateOn(obj, loop);

                            if (!isRunning) goto EXIT;
                            if (!isEnabled) break;
                        }
                    }
                }
            }

        EXIT:
            var endLoop = GenerateNativeloop();
            lock (_nativeLock)
            {
                foreach (var obj in allNativeObjects)
                {
                    TryCallEndOn(obj, endLoop);
                }
            }
        }
        DateTime lastCall;
        private NativeLoop GenerateNativeloop()
        {
            var timeNow = DateTime.UtcNow;
            var deltaTime = timeNow - lastCall;

            lastCall = timeNow;
            return new NativeLoop(deltaTime.TotalSeconds, ++loopsCount);
        }

        private void TryCallUpdateOn(INative obj, NativeLoop loop)
        {
            if (obj is null)
                return;

            try
            {
                obj.Update(loop);
            }
            catch(Exception e)
            {
                UnityEngine.Debug.LogError(e);
            }
        }
        private void TryCallEndOn(INative obj, NativeLoop loop)
        {
            if (obj is null)
                return;

            try
            {
                obj.Update(loop);
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError(e);
            }
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (isRunning)
                    Terminate();

                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                    allNativeObjects = null;
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        ~NativeThread()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(false);
        }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    public interface INative
    {
        void Start();
        void Update(NativeLoop loop);
        void End(NativeLoop loop);
    }

    public interface IExecutable
    {
        void Execute();
        void Terminate();
    }

    public readonly ref struct NativeLoop
    {
        public NativeLoop(double deltaTime, ulong loopID) : this()
        {
            this.deltaTime = deltaTime;
            this.loopID = loopID;
        }

        public double deltaTime { get; }
        public ulong loopID { get; }

    }

    //new
    public class NativeCode<T> : INative where T : class, INative
    {
        protected T self;
        private NativeThread _selfThread;
        protected NativeThread SelfThread => _selfThread;

        void INative.End(NativeLoop loop)
        {
            self.End(loop);
        }

        void INative.Start()
        {
            self.Start();
        }

        void INative.Update(NativeLoop loop)
        {
            self.Update(loop);
        }

        public NativeCode(NativeThread requestThread, T wrap)
        {
            _selfThread = requestThread;
            self = wrap;
            ApplyToThreadAsync();
        }

        async void ApplyToThreadAsync()
        {
            await Task.Run(delegate {
                _selfThread.AddNative(this);
            });
        }
    }
}

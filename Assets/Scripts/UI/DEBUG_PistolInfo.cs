﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DEBUG_PistolInfo : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    [SerializeField] Actors.Weapons.Pistol pistol;

    private void Update()
    {
        var magazine = pistol.Magazine;
        string reload = "";
        if (pistol.IsReloaded)
            reload = $"[Reloading in {magazine.reloadTime - pistol.ReloadTime:f1}s]";


        text.text = $"{magazine.CurAmmo} / {magazine.MaxAmmo} [{pistol.TotalAmmo}] {reload}";
    }
}

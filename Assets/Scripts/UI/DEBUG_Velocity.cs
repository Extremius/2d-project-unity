﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DEBUG_Velocity : MonoBehaviour
{
    [SerializeField] private Actors.MoveBehaviour target;
    [SerializeField] private Actors.ActorBody body;
    [SerializeField] private TextMeshProUGUI info;
    [SerializeField] private TextMeshProUGUI inputInfo;
    System.Diagnostics.Stopwatch watcher = System.Diagnostics.Stopwatch.StartNew();
    private void Update()
    {
        if(target != null)
        {
            info.text = $"{target.name}\nTime: {watcher.ElapsedMilliseconds} ms\nVelocity: {target.Velocity.ToString()}\nDirection: {target.Velocity.normalized.ToString()}\nSpeed: {target.Velocity.magnitude:f1}\nOnGround: {target.OnGround}\nIsShifting: {target.IsShifting}";
        }
        if(inputInfo!=null)
        {
            Vector2 directionInput = new Vector2(Input.GetAxis(Actors.MoveBehaviour.moveAxis), Input.GetAxis(Actors.MoveBehaviour.jumpAxis));
            inputInfo.text = $"Input: {directionInput.ToString()}";
        }
        if(body != null)
        {
            info.text += $"\nResistance: {(body.LastResistance * 100):f0}\nDamage: {body.LastDamage}";
        }
    }
}

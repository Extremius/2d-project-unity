﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace DebugEditor.UI
{
    public class FrameRate : MonoBehaviour
    {
        [SerializeField] private FpsColor badColor, normalColor, goodColor;
        [SerializeField] TextMeshProUGUI output;
        //[SerializeField, TextArea(1, 6)] string outputFormat = "FPS {0}";

        private float minFps, maxFps, summaryFps;
        private int totalFrames = 0;
        private float lastTime;
        private void Update()
        {
            var passedTime = Time.unscaledTime - lastTime;
            if (passedTime>1)
            {
                var averageFps = summaryFps / totalFrames;
                Color forAvr, forMax, forMin;

                forAvr = ChoseColor(averageFps);
                forMax = ChoseColor(maxFps);
                forMin = ChoseColor(minFps);

                Color ChoseColor(float fps)
                {
                    if (fps >= goodColor.fromFps)
                        return goodColor.color;
                    else if (fps >= normalColor.fromFps)
                        return normalColor.color;
                    else return badColor.color;
                }

                output.SetText($"FPS <color=#{ColorUtility.ToHtmlStringRGBA(forMin)}>{minFps:f0}</color> / <color=#{ColorUtility.ToHtmlStringRGBA(forAvr)}>{averageFps:f0}</color> / <color=#{ColorUtility.ToHtmlStringRGBA(forMax)}>{maxFps:f0}</color>");

                totalFrames = 0;
                summaryFps = 0;
                lastTime = Time.unscaledTime;
                minFps = float.MaxValue;
                maxFps = float.MinValue;
            }
            else
            {
                var fps = 1f / Time.unscaledDeltaTime;

                if (minFps > fps)
                    minFps = fps;
                else if (maxFps < fps)
                    maxFps = fps;

                summaryFps += fps;
                totalFrames++;
            }
        }

        [System.Serializable]
        public struct FpsColor
        {
            public float fromFps;
            public Color color;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Actors.UI
{
    public class Healthbar : MonoBehaviour
    {
        [SerializeField] private HealthBody target;
        [SerializeField] private Slider sld;
        [SerializeField] private TextMeshProUGUI healthText;

        private void Update()
        {
            if(target is null)
            {
                gameObject.SetActive(false);
                return;
            }
            var healthAccess = target.HealthAccess;
            sld.value = (float)healthAccess.Percent;
            healthText.text = $"{healthAccess.Value:f0}";
        }
    }
}

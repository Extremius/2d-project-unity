﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors;
using UnityEngine.UI;
using TMPro;
using Actors.Weapons;

namespace Actors.UI
{
    public class PlayerInform : MonoBehaviour
    {
        //components
        [SerializeField,TextArea(1,8)] string hpName = "hp {0}", spName = "sp {0}", lvName = "lvl\n{0}", gunName="GUN\n{0}\n{1}";

        [SerializeField] GameObject target;
        ProgressionManager prgmng;
        ActorBody health;
        StaminaBody stamina;
        BulletWeapon weapon;
        [SerializeField] GameObject upPanel;
        [SerializeField] Slider expSlider, healthSlider, staminaSlider, armorSlider, gunSlider;
        [SerializeField] TextMeshProUGUI lvlInfo, healthInfo, staminaInfo, expInfo, weaponInfo;
        //Stamina
        [SerializeField] private Color canUse, cantUse;
        private bool isExh = false;

        //for interpolate
        [SerializeField] private float speedInterpolation = 6;
        private double expInterpolate, weaponInterpolate;
        private void Start()
        {
            prgmng = target.GetComponent<ProgressionManager>();
            health = target.GetComponent<ActorBody>();
            stamina = target.GetComponent<StaminaBody>();
            weapon = target.GetComponent<WeaponHolder>().WeaponObject.GetComponent<BulletWeapon>();
        }

        private bool showedUpPanel = false;
        private void Update()
        {
            healthSlider.value = (float)health.HealthAccess.Percent;
            staminaSlider.value = (float)stamina.StaminaAccess.Percent;
            //expSlider.value = (float)prgmng.Expirience.Percent;
            armorSlider.value = (float)health.ArmorAccess.Percent;

            expInterpolate += (prgmng.Expirience.Percent - expInterpolate) * Time.unscaledDeltaTime * speedInterpolation;
            expSlider.value = (float)expInterpolate;

            healthInfo.text = string.Format(hpName, health.HealthAccess.Value.ToString("f0"));
            staminaInfo.text = string.Format(spName, stamina.StaminaAccess.Value.ToString("f0"));
            lvlInfo.text = string.Format(lvName, prgmng.LvlInfo.ToString("f0"));

            //stamina
            if (isExh != stamina.IsExhaustion)
            {
                isExh = stamina.IsExhaustion;
                if (isExh)
                {
                    staminaSlider.fillRect.GetComponent<Image>().color = cantUse;
                }
                else
                {
                    staminaSlider.fillRect.GetComponent<Image>().color = canUse;
                }
            }

            //weapon
            if (weapon != null)
            {
                if (!weapon.IsReloaded)
                {
                    if (weapon.TotalAmmo is 0 && weapon.Magazine.IsEmpty)
                    {
                        weaponInterpolate = 0;
                        weaponInfo.text = "***\nEMPTY\n***";
                    }
                    else
                    {
                        weaponInterpolate += (weapon.Magazine.Percent - weaponInterpolate) * Time.unscaledDeltaTime * speedInterpolation;
                        var magazine = weapon.Magazine;
                        weaponInfo.text = string.Format(gunName, $"{magazine.CurAmmo}/{magazine.MaxAmmo}", $"[{weapon.TotalAmmo}]");
                    }
                }
                else
                {
                    weaponInterpolate = weapon.ReloadProgress;
                    weaponInfo.text = string.Format(gunName, $"RELOAD", $"[{weapon.TotalAmmo}]");
                }
                gunSlider.value = (float)weaponInterpolate;
            }
            else
            {
                gunSlider.value = 0;
                weaponInfo.text = "UNSUPPORTED\nGUN";
            }

            //statup
            if(prgmng.Points>0 && !showedUpPanel)
            {
                upPanel.gameObject.SetActive(true);
                showedUpPanel = true;
            }
            else if(prgmng.Points<1 && showedUpPanel)
            {
                upPanel.gameObject.SetActive(false);
                showedUpPanel = false;
            }
        }

        public void UpStat(string statName)
        {
            prgmng.SetPointsInStat(1, statName);
        }
    }
}

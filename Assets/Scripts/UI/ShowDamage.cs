﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;
using TMPro;

namespace Actors.UI
{
    public class ShowDamage : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI ui;
        [SerializeField] double showTime = 4;
        Stopwatch watch = Stopwatch.StartNew();
        private double totalDamage;
        private bool isShowing = false;
        private void Update()
        {
            if(isShowing)
            {
                var elapsed = watch.Elapsed;
                if(elapsed.TotalSeconds > showTime)
                {
                    ui.gameObject.SetActive(false);
                    isShowing = false;
                    totalDamage = 0;
                }
                else
                {
                    var alpha = 1f - (elapsed.TotalSeconds / showTime);
                    var color = ui.color;
                    color.a = (float)alpha;
                    ui.color = color;
                }
            }
        }

        public void PrintDamage(double addDamage)
        {
            if(!isShowing)
            {
                ui.gameObject.SetActive(true);
            }
            isShowing = true;
            var color = ui.color;
            color.a = 1; ui.color = color;
            watch.Restart();

            totalDamage += addDamage;
            ui.text = $"{totalDamage:f0} hp";
        }
    }
}

﻿using Actors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



namespace Actors.UI
{
    public class Staminabar : MonoBehaviour
    {



        [SerializeField] private StaminaBody target;
        [SerializeField] private Slider sld;
        [SerializeField] private TextMeshProUGUI healthText;
        [SerializeField] private Image fill;
        [SerializeField] private Color canUse, cantUse;

        private bool isExh;

        private void Update()
        {
            
            var staminaAccess = target.StaminaAccess;
            sld.value = (float)staminaAccess.Percent;
            healthText.text = $"{staminaAccess.Value:f0}";

            if(isExh != target.IsExhaustion)
            {
                isExh = target.IsExhaustion;
                if(isExh)
                {
                    fill.color = cantUse;
                }
                else
                {
                    fill.color = canUse;
                }
            }
        }



    }


}
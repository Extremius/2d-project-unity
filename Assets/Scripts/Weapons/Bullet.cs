﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Actors.Weapons
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private double damage;
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] GameObject owner;
        public event Action<HealthBody> OnKillHandler;
        public bool IsActive => gameObject.activeSelf;
        public void Shoot(Vector2 pos, Vector2 force, double damage, GameObject owner, float mass =1)
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), owner.GetComponent<Collider2D>(), true);

            gameObject.SetActive(true);
            transform.position = pos;

            rb.mass = mass;

            this.damage = damage;
            this.owner = owner;
            rb.AddForce(force, ForceMode2D.Impulse);
           
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.rigidbody != null)
            {
                if(collision.rigidbody.gameObject != owner)
                {
                    if(collision.rigidbody.TryGetComponent(out HealthBody body))
                    {
                        ApplyDamage(body);
                    }
                }
            }

            Clear();
        }

        void Clear()
        {
            if(owner != null)
                Physics2D.IgnoreCollision(GetComponent<Collider2D>(), owner.GetComponent<Collider2D>(), false);
            OnKillHandler = null;
            gameObject.SetActive(false);
            owner = null;
            GetComponentInChildren<TrailRenderer>().Clear();
        }

        void ApplyDamage(HealthBody body)
        {
            var wasAlive = body.IsAlive;
            body.ChangeHealth(owner, -damage);
            //Debug.Log($"Print damage on {body.name}");
            if (wasAlive && !body.IsAlive)
            {
                OnKillHandler?.Invoke(body);

            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Enviroment.PoolSupport;
using Stopwatch =  System.Diagnostics.Stopwatch;

namespace Actors.Weapons
{
    [System.Serializable]
    public struct Magazine
    {
        #region fields
        [SerializeField] private int maxAmmo;
        public double reloadTime;
        private int curAmmo;
        #endregion

        public int MaxAmmo
        {
            get => maxAmmo;
            set
            {
                if (curAmmo > value)
                    curAmmo = value;
                maxAmmo = value;
            }
        }

        public int CurAmmo
        {
            get => curAmmo;
            set
            {
                if (value < 0)
                    value = 0;
                if (value > maxAmmo)
                    value = maxAmmo;
                curAmmo = value;
            }
        }

        public bool IsEmpty => curAmmo is 0;
        public bool IsFullLoad => curAmmo == maxAmmo;
        public bool HasAmmo => !IsEmpty;
        public float Percent => (float)curAmmo / maxAmmo;
        public Magazine(double reloadTime, int maxAmmo, bool isLoaded)
        {
            this.reloadTime = reloadTime;
            this.maxAmmo = maxAmmo;
            if (isLoaded)
                curAmmo = maxAmmo;
            else curAmmo = 0;
        }

        #region methods
        public int Reload(int inAmmo)
        {
            var delta = maxAmmo - curAmmo;
            inAmmo -= delta;
            if (inAmmo < 0)
            {
                delta += inAmmo;
                inAmmo = 0;
            }

            CurAmmo += delta;
            return inAmmo;
        }
        #endregion
    }

    public abstract class BulletWeapon : MonoBehaviour, IWeapon
    {
        //Сценозависимые
        [SerializeField] WeaponHolder holder;

        //Не сценозависимые
        [SerializeField] Data.BulletWeaponData globalData;

        Bullet prefab;
        float force;
        double damage;
        double firerateInMinutes;

        int ammoPerShot;
        double deltaAngle;
        Magazine magazine;

        //полностью приватные
        Stopwatch reloadWatch = new Stopwatch();
        private bool isReloaded = false;
        Pool<Bullet> pool;
        Stopwatch timeWatch = Stopwatch.StartNew();
        AmmoHold ammo;

        #region props
        public int TotalAmmo => ammo.Count;
        public Magazine Magazine => magazine;
        public bool IsReloaded => isReloaded;
        public double ReloadTime => reloadWatch.Elapsed.TotalSeconds;
        public float ReloadProgress => (float)(ReloadTime / magazine.reloadTime);

        private double fireDelay => 1000d / (firerateInMinutes * 0.016666d);
        #endregion

        private void ApplyData()
        {
            if(globalData == null)
            {
                Debug.LogError("Weapon Data is missing!");
                return;
            }
            prefab = globalData.Prefab;
            force = globalData.Force;
            damage = globalData.Damage;
            firerateInMinutes = globalData.FirerateInMinutes;
            ammo = holder.GetAmmo(globalData.Ammo);
            ammoPerShot = globalData.AmmoPerShot;
            deltaAngle = globalData.DeltaAngle;
            magazine = globalData.Magazine;
        }

        #region unity
        private void Start()
        {
            ApplyData();

            pool = Enviroment.GlobalEnviroment.Self.PoolManager.GetPool(prefab);
            magazine.CurAmmo = 0;
            ammo.Count = magazine.Reload(ammo.Count);
        }

        private void Update()
        {
            if (isReloaded)
            {
                if (reloadWatch.Elapsed.TotalSeconds > magazine.reloadTime)
                {
                    ammo.Count = magazine.Reload(ammo.Count);
                    reloadWatch.Reset();
                    isReloaded = false;
                }
            }
        }
        #endregion

        #region behaviour
        protected virtual Bullet GetBullet()
        {
            return pool.Object();
        }

        protected virtual bool CanFire()
        {
            if (isReloaded)
                return false;

            if (magazine.IsEmpty)
            {
                Reload();
                return false;
            }

            if (timeWatch.Elapsed.TotalMilliseconds >= fireDelay)
            {
                timeWatch.Restart();
                return true;
            }
            else return false;
        }

        protected virtual void DelegateKill(HealthBody body)
        {
            holder.OnKill(body);
        }

        public virtual void Reload()
        {
            if (TotalAmmo < 1)
                return;

            if (!isReloaded)
            {
                if (magazine.IsFullLoad)
                    return;

                isReloaded = true;
                reloadWatch.Start();
            }
        }

        void IWeapon.Fire()
        {
            Shout();
        }

        protected virtual void Shout()
        {
            if (!CanFire())
                return;

            for (int i = 0; i < ammoPerShot; i++)
            {
                var bullet = GetBullet();


                if (bullet != null)
                {
                    bullet.OnKillHandler += DelegateKill;
                    var trueDir = holder.Direction;

                    var baseAngle = Vector2.SignedAngle(Vector2.right, trueDir);

                    var deltaRandomAngle = UnityEngine.Random.Range(-(float)deltaAngle * 0.5f, (float)deltaAngle * 0.5f);
                    var rad = Mathf.Deg2Rad * (baseAngle + deltaRandomAngle);

                    Vector2 deltaDirection;
                    deltaDirection.x = Mathf.Cos(rad);
                    deltaDirection.y = Mathf.Sin(rad);


                    bullet.Shoot(holder.ShootPosition, deltaDirection * force, damage, holder.gameObject, globalData.Mass);
                }
            }
            magazine.CurAmmo -= ammoPerShot;
        }
       
        #endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Actors.Weapons.Data
{

    [CreateAssetMenu(fileName = "new bulletdata", menuName = "Weapons/New Bullet Data")]
    public class BulletWeaponData : ScriptableObject
    {
        [SerializeField] Bullet prefab;
        [SerializeField] string ammo = "undefinited";
        [SerializeField] float force = 30;
        [SerializeField] float mass = 0.2f;
        [SerializeField] double damage = 25;
        [SerializeField] double firerateInMinutes = 300;
        [SerializeField] int ammoPerShot = 1;
        [SerializeField] double deltaAngle = 1;
        [SerializeField] Magazine magazine = new Magazine(5, 8, true);

        #region read
        public Bullet Prefab => prefab;
        public float Force => force;
        public float Mass => mass;
        public double Damage => damage;
        public double FirerateInMinutes => firerateInMinutes;
        public string Ammo => ammo;
        public int AmmoPerShot => ammoPerShot;
        public double DeltaAngle => deltaAngle;
        public Magazine Magazine => magazine;
        #endregion
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enviroment.PoolSupport;

using Stopwatch = System.Diagnostics.Stopwatch;


namespace Actors.Weapons
{

    public class Pistol : BulletWeapon
    {
        public int totalkill = 0;

        protected override void DelegateKill(HealthBody body)
        {
            base.DelegateKill(body);
            totalkill++;
        }
    }
}
